/*
 * Copyright (c) 2020, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */
 
public with sharing class GuestDocumentUploadController {
    @AuraEnabled
    public static List<DocumentWrapper> getAllDocuments(String recordId) {
        List<DocumentWrapper> documents = new List<DocumentWrapper>();
        Map<String, List<ContentVersion>> groupedDocuments = groupDocumentsByType(recordId);
        List<Guest_Flow_Document__mdt> documentTypes = [
                SELECT Id, Category__r.MasterLabel, Document_Name__c, Score__c, Description__c, Icon__c,
                        Accepted_File_Format__c, File_Link__c, Mandatory__c, Order__C
                FROM Guest_Flow_Document__mdt
                WHERE Active__c = true
                WITH SECURITY_ENFORCED
                ORDER BY Mandatory__c DESC, Order__C ASC
        ];
        for (Guest_Flow_Document__mdt docType : documentTypes) {
            DocumentWrapper wrapper = new DocumentWrapper();
            wrapper.document = docType;
            if (groupedDocuments.containsKey(docType.Id)) {
                wrapper.uploadedDocuments.addAll(groupedDocuments.get(docType.Id));
                documents.add(wrapper);
            }
        }
        System.Debug(documents);
        return documents;
    }

    @AuraEnabled
    public static List<DocumentWrapper> getDocuments(String recordId, String category) {
        List<DocumentWrapper> documents = new List<DocumentWrapper>();
        Map<String, List<ContentVersion>> groupedDocuments = groupDocumentsByType(recordId);

        List<Guest_Flow_Document__mdt> documentTypes = [
                SELECT Id, Category__r.MasterLabel, Document_Name__c, Score__c, Description__c, Icon__c,
                        Accepted_File_Format__c, File_Link__c, Mandatory__c, Order__C
                FROM Guest_Flow_Document__mdt
                WHERE Category__r.MasterLabel = :category AND Active__c = true
                WITH SECURITY_ENFORCED
                ORDER BY Mandatory__c DESC, Order__C ASC
        ];
        for (Guest_Flow_Document__mdt docType : documentTypes) {
            DocumentWrapper wrapper = new DocumentWrapper();
            wrapper.document = docType;
            if (groupedDocuments.containsKey(docType.Id)) {
                wrapper.uploadedDocuments.addAll(groupedDocuments.get(docType.Id));
            }
            documents.add(wrapper);
        }
        return documents;
    }

    @AuraEnabled
    public static void updateDocuments(String documentTypeId, List<String> documentIds) {
        List<Content_Version_Document_Type__c> cvDocTypes = new List<Content_Version_Document_Type__c>();
        
        List<ContentVersion> uploadedDocuments = [
            SELECT Id
            FROM ContentVersion
            WHERE ContentDocumentId IN :documentIds
            WITH SECURITY_ENFORCED
        ];
        
        //As we are not allowed to update record under guest license, 
        //insert a one-one mapping Content Version Document Type
        Content_Version_Document_Type__c tempcvDocType;
        for (ContentVersion doc : uploadedDocuments) {
            tempcvDocType = new Content_Version_Document_Type__c();
            tempcvDocType.Document_Type_Id__c=documentTypeId;
            tempcvDocType.Content_Version_ID__c=doc.Id;
            cvDocTypes.add(tempcvDocType);
        }

        insert cvDocTypes;
    }

    /**
     * Since we are using guest license, we will not use delete document method 
     */
//    @AuraEnabled
//    public static void deleteDocument(String documentId) {
//        if (String.isNotBlank(documentId)) {
//            List<ContentDocument> documents = [SELECT Id FROM ContentDocument WHERE Id = :documentId];
//            delete documents;
//        }
//    }

    private static List<String> retrieveRelatedDocumentIds(String recordId) {
        List<ContentDocumentLink> documentLinks = [
                SELECT ContentDocumentId
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :recordId
                WITH SECURITY_ENFORCED
        ];
        List<String> documentIds = new List<String>();
        for (ContentDocumentLink documentLink : documentLinks) {
            documentIds.add(documentLink.ContentDocumentId);
        }
        return documentIds;
    }

    private static Map<String, List<ContentVersion>> groupDocumentsByType(String recordId) {
        Map<String, List<ContentVersion>> groupedDocuments = new Map<String, List<ContentVersion>>();

        if (String.isNotBlank(recordId)) {
            
            List<String> relativeDocumentIds = retrieveRelatedDocumentIds(recordId);
            List<ContentVersion> uploadedDocuments = [
                    SELECT Id, ContentDocumentId, Title, Description,
                            FileType, FileExtension, LastModifiedDate
                    FROM ContentVersion
                    WHERE ContentDocumentId IN :relativeDocumentIds
                    WITH SECURITY_ENFORCED
            ];
            
            Map<ID, ContentVersion> mapCVs = 
                new Map<ID, ContentVersion>(uploadedDocuments);
            System.debug('==========================');
            System.debug('mapCVs - '+ mapCVs);
            System.debug('mapCVs .keySet() - '+ mapCVs.keySet());
            System.debug('==========================');
            
            //It is storing record id as key instead of Content_Version_ID__c
            List<Content_Version_Document_Type__c> listCVDTs = [SELECT Content_Version_ID__c, Document_Type_Id__c 
                     FROM Content_Version_Document_Type__c
                     WHERE Content_Version_ID__c IN :mapCVs.keySet()
                    	WITH SECURITY_ENFORCED
                    ];
            
            Map<Id, Content_Version_Document_Type__c> mapCVDTs = new Map<Id,Content_Version_Document_Type__c>();
            for (Content_Version_Document_Type__c cvdt : listCVDTs) {
				mapCVDTs.put(cvdt.Content_Version_ID__c,cvdt);
            }
            
            System.debug('==========================');
            for (ContentVersion cv : uploadedDocuments) {
                System.debug('groupDocumentsByType - CV.ID = ' + cv.id);
                System.debug('mapCVDTs.get(cv.id) = '+mapCVDTs.get(cv.id));
                if(mapCVDTs.get(cv.id)!=null){
                    System.debug('mapCVDTs.get(cv.id).Document_Type_Id__c = '+mapCVDTs.get(cv.id).Document_Type_Id__c);
                    String documentTypeId = mapCVDTs.get(cv.id).Document_Type_Id__c;
                    if (!groupedDocuments.containsKey(documentTypeId)) {
                        groupedDocuments.put(documentTypeId, new List<ContentVersion>());
                    }
                    groupedDocuments.get(documentTypeId).add(cv);
                }
            }
            System.debug('==========================');
        }
        return groupedDocuments;
    }

    public class DocumentWrapper {
        @AuraEnabled
        public Guest_Flow_Document__mdt document;

        @AuraEnabled
        public List<ContentVersion> uploadedDocuments;

        public DocumentWrapper() {
            uploadedDocuments = new List<ContentVersion>();
        }
    }

//    public static Boolean getCustomLabels() {
//        String promptMessage = Label.Cmp_PromptMessage;
//        String promptTitle = Label.Cmp_PromptTitle;
//        String promptNegativeButtonText = Label.Cmp_PromptNegativeButtonText;
//        String promptPositiveButtonText = Label.Cmp_PromptPositiveButtonText;
//        return true;
//    }

}