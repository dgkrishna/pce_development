@isTest
public class zTEST_Agent_Inquiry {

    @isTest static void testMyController() {
		Profile pAg = [SELECT Id, Name FROM Profile WHERE Name = 'SMART Agency User'];
		Id saveId = pAg.Id;

		// UserRole urol = new UserRole(
		// 	Name = 'The New Kid In Town Partner Manager'
		// );
		// insert urol;

		//create TSM User
			Profile prof = [Select Id from Profile where name = 'SMART Territory Sales Manager User Profile'];
			UserRole uRole = [Select Id from UserRole where name = 'SMART PFS TSM 01'];
			User usr = new User(
				ProfileId 		  = prof.Id,
				UserRoleId 		  = uRole.Id,
				Username 		  = System.now().millisecond() + 'xtest2@test.com',
				Alias 			  = 'xbatman',
				Email 			  = 'xxhdkdjhasedfasdfbruce.wayne@wayneenterprises.com',
				EmailEncodingKey  = 'UTF-8',
				Firstname 		  = 'Bruce',
				Lastname 		  = 'Wayne',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey 	  = 'en_US',
				TimeZoneSidKey 	  = 'Asia/Manila'
			);
			insert usr;
			Id withRoleId = usr.Id;

		//secondary System Administrator user to run the test
			prof = [Select Id from Profile where name = 'System Administrator'];
			User usr2 = new User();
				usr2.ProfileId 		   = prof.Id;
				usr2.Username  		   = System.now().millisecond() + 'xtest3@test.com';
				usr2.Alias 	  		   = 'thor';
				usr2.Email 			   = 'jfiejdkurldilejhjhfclark.kent@metroglobe.com';
				usr2.EmailEncodingKey  = 'UTF-8';
				usr2.Firstname 		   = 'Bruce';
				usr2.Lastname 		   = 'Wayne';
				usr2.LanguageLocaleKey = 'en_US';
				usr2.LocaleSidKey 	   = 'en_US';
				usr2.TimeZoneSidKey    = 'Asia/Manila';
			insert usr2;

		System.runAs(usr2) {
			Test.StartTest();
				zzAgent_Inquiry controller = new zzAgent_Inquiry();

				controller.AgentEmail = '';
				controller.haveUserList();

				controller.AgentEmail = 'kent';
				controller.haveUserList();

				//create Account and update it to enable parter
					Account acc = new Account(
						Name 		= 'The New Kid In Town',
						BillingCity = 'Makati Town',
						Phone 		= '8865-4020',
						OwnerId 	= withRoleId
					);
					insert acc;

					acc.IsPartner = true;
					update acc;

					Account acc2 = new Account(
						Name 		= 'The Old Kid In Town',
						BillingCity = 'Makati City',
						Phone 		= '8865-4121',
						OwnerId 	= withRoleId
					);
					insert acc2;

					acc2.IsPartner = true;
					update acc2;

				//create Contact/Partner user
					Contact cont = new Contact(
						AccountId = acc.Id,
						Email 	  = 'xyhaldfhlclarkkent@metropolitan.com',
						FirstName = 'Clarky',
						LastName  = 'Kenty',
						Phone 	  = '09123456789',
						Agent_Code__c = '654321',
						Agent_TSM__c  = acc.OwnerId,
						OwnerId 	  = acc.OwnerId
					);
					insert cont;

					Integer subslen;
					if (cont.LastName.length() < 4)
						subslen = cont.LastName.length();
					else
						subslen = 4;

					usr = new User(
						ProfileId = saveId,
						ContactId = cont.Id,
						FirstName = cont.FirstName,
						LastName  = cont.LastName,
						Username  = cont.Email,
						Email = cont.Email,
						Alias = cont.FirstName.substring(0, 1).toLowercase() + cont.LastName.substring(0, subslen).toLowercase(),
						CommunityNickname = cont.FirstName + cont.LastName.substring(0, 1),
						EmailEncodingKey  = 'UTF-8',
						TimeZoneSidKey 	  = 'Asia/Manila',
						LocaleSidKey 	  = 'en_US',
						LanguageLocaleKey = 'en_US',
						UserPermissionsMarketingUser = true,
						IsActive = true
					);
					insert usr;

					Id saveUserId = usr.Id;

				controller.saveUserId = saveUserId;
				controller.testFutureHelper();

				controller.AgentEmail = 'kent';
				controller.haveUserList();

				//create 2nd Contact/Partner user
					cont = new Contact(
						AccountId = acc.Id,
						Email 	  = 'xyhaldfhlclarkkent22@metropolitan.com',
						FirstName = 'Clarky2',
						LastName  = 'Kenty2',
						Phone 	  = '09123456782',
						Agent_Code__c = '6543212',
						Agent_TSM__c  = acc.OwnerId,
						OwnerId 	  = acc.OwnerId
					);
					insert cont;

					if (cont.LastName.length() < 4)
						subslen = cont.LastName.length();
					else
						subslen = 4;

					usr = new User(
						ProfileId = saveId,
						ContactId = cont.Id,
						FirstName = cont.FirstName,
						LastName  = cont.LastName,
						Username  = cont.Email,
						Email = cont.Email,
						Alias = cont.FirstName.substring(0, 1).toLowercase() + cont.LastName.substring(0, subslen).toLowercase(),
						CommunityNickname = cont.FirstName + cont.LastName.substring(0, 1),
						EmailEncodingKey  = 'UTF-8',
						TimeZoneSidKey 	  = 'Asia/Manila',
						LocaleSidKey 	  = 'en_US',
						LanguageLocaleKey = 'en_US',
						UserPermissionsMarketingUser = true,
						IsActive = true
					);
					insert usr;
					saveId = usr.Id;

				controller.AgentEmail = 'kent';
				controller.haveUserList();

				controller.newData = saveId;
				controller.getListRow();

				controller.resetPassword();

				controller.newData = '6123456';
				controller.updateAgentCode();

				controller.newData = 'xyz@zyxwvu.com';
				controller.updateAgentEmail();

				controller.EmailTp = 'EVerify';
				controller.EmailTo = 'abcde1234@moc.com';
				controller.EmailCC = 'abcde1234@moc.com';
				controller.sendUserEmail();

				//obtain list of new accounts for transfer
					List<Account> accList = [SELECT Id, Name, OwnerId FROM Account WHERE Id !=: acc.Id ORDER BY Name];
					String AccName = null;
					String AccRId;
					String AccOwn;

					if (accList.size() > 0) {
						for (Account ac : accList) {
							if (AccName != null) {
								AccName += '~' + ac.Name;
								AccRId  += '~' + ac.Id;
								AccOwn  += '~' + ac.OwnerId;
							}
							else {
								AccName = ac.Name;
								AccRId  = ac.Id;
								AccOwn  = ac.OwnerId;
							}
						}
					}

				controller.rIndex = 1;
				controller.transferAgent();

				controller.saveUserId = saveUserId;
				controller.deactivateUser();

				controller.xCancel();
			Test.StopTest();
		}
    }
}