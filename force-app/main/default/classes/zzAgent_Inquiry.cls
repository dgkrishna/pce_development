public class zzAgent_Inquiry {
	//public variables
		//screen display control boolean variables
			public boolean showPromptAgent {get; set;}
			public boolean showAgencyUList {get; set;}
			public boolean showUserDetails {get; set;}
			public boolean showErorMessage {get; set;}
			public boolean showSuccessTran {get; set;}
			public boolean showSuccessDeac {get; set;}
		public List<User> usrList {get; set;}	//used by both pageblockTables in VFP (1. multiple hits and 2. other users of same agency)
		//user details for display
			public String saveFirstName {get; set;}
			public String saveLastName  {get; set;}
			public String saveEmail		{get; set;}
			public String saveUserName  {get; set;}
			public String saveProfile	{get; set;}
			public String saveLastLogin {get; set;}
			public String saveAgencyName {get; set;}
			public String saveAgencyCode {get; set;}
			public String saveTSMName	{get; set;}
			public String saveTSMEmail	{get; set;}
		//string passed to display agency list for selection in the transfer agent feature
		public String AccName {get; set;}
		//accompanying strings used to process transfer of agent -- using index of selected agency
		public String AccOwn;
		public String AccRId;
		//receiver variables
			public String AgentEmail {get; set;}
			public String newData {get; set;}
			public Integer rIndex {get; set;}
			public String EmailTp {get; set;}
			public String EmailTo {get; set;}
			public String EmailCC {get; set;}
		//storage variables
			public Id saveUserId;
			public Id saveAcctId;
		//to expose contact soql result to multiple methods
		public Contact cont;

	public zzAgent_Inquiry() {
		//we inititalize or display control variables and the search Key
		showPromptAgent = true;
		showAgencyUList = false;
		showUserDetails = false;
		showErorMessage = false;
		showSuccessTran = false;
		showSuccessDeac = false;
		AgentEmail = '';
	}

	public void haveUserList() {
		showErorMessage = false;	//reset error msg display boolean

		Boolean xError = false;
		if (AgentEmail == '')
			xError = true;			//we don't accept blank Email Address!! - display an error message
		else {
			//we will always attempt to query a List of users -- We will use the AgentEmail as Email or First Name or Last Name or Agency Name search key
			String query =  'SELECT Id, FirstName, LastName, Email, Profile.Name, Contact.Id, LastLoginDate, Account.Name, Account.Id, Contact.Agent_Code__c, Username ' +
							'FROM User WHERE Profile.Name LIKE \'%SMART Agency%\' AND IsActive = true AND ' +
							'(Email LIKE \'%' + AgentEmail + '%\' OR FirstName LIKE \'%' + AgentEmail + '%\' OR LastName LIKE \'%' +
								AgentEmail + '%\' OR Account.Name LIKE \'%' + AgentEmail + '%\') ' +
							'ORDER BY Account.Name ASC, FirstName ASC, LastName ASC';

			usrList = Database.query(query);	//if there is a need to assemble the query because of variables.. we use Database.query
			System.Debug('usrList.size() :>> ' + usrList.size());
			if (usrList.size() == 0) {
				xError = true;		//if we didn't get any user, we display an error message
			}
		}

		if (xError) {
			//if we have an error.. enable display of the message line
			showErorMessage = true;
			//PageReference RetPage = ApexPages.currentPage();
		}
		else {
			if (usrList.size() > 1)
				showAgencyUList = true;		//if more than one user - display a list first for selection
			else {
				showUserDetails = true;		//otherwise.. go directly to the user details display
				showDetails(0);				//the index we pass is 0 because we only have one record
			}

			showPromptAgent = false;		//we close the first section of our page
		}
	}

	public void getListRow() {
		//if we came from a selection in the user list, we get the index of usrList via long way
		//	hahaha - tagalan ako maghanap ng way para makuha yong row index ng apex:pageBlockTable
		//	pag alam ko na sya.. diretso na tayo sa showDetails(index) hehehehehehe
		//CALLED BY BOTH Select and Switch LINKS of the user list tables -- the Id is passed to get the index
		System.Debug('newData :>> ' + newData);
		Integer ndx = 0;
		for (User ul : usrList) {
			if (String.valueOf(ul.Id) == newData) {
				showDetails(ndx);
				showAgencyUList = false;
				showUserDetails = true;
				break;
			}

			ndx++;
		}
	}

	public void showDetails (Integer xrow) {
		System.Debug('we are in showDetails!');
		//and this is where we display the user details together with data on all the other users in the same
		//	Agency of the subject user
			saveUserId    = usrList[xrow].Id;			//we will use this for resetPassword and Deactivate later
			saveAcctId    = usrList[xrow].Account.Id;	//used to filter agency in detail from appearing in the selection list
			saveFirstName = usrList[xrow].FirstName;
			saveLastName  = usrList[xrow].LastName;
			saveEmail	  = usrList[xrow].Email;
			saveUserName  = usrList[xrow].Username;
			saveProfile   = usrList[xrow].Profile.Name;
			saveLastLogin = String.valueOf(Datetime.valueOf(usrList[xrow].LastLoginDate));

		//we read the Contact record of the Partner User to get the Account Name (Agency Name) and the TSM in-charge!
		//	this will be updated later if the transferAgent method is called
		//	the FirstName, LastName and Email are for system.debug display only later in transfer agent method
		cont = [SELECT Id, FirstName, LastName, Email, Agent_Code__c, Account.Name, Owner.Name, Owner.Email
					FROM Contact WHERE Id =: usrList[xrow].Contact.Id];
		saveAgencyName = cont.Account.Name;
		saveAgencyCode = cont.Agent_Code__c;
		saveTSMName    = cont.Owner.Name;
		saveTSMEmail   = cont.Owner.Email;

		//then we get the records of all the other users in the same agency -- minus the subject user OF COURSE!!
		//IF OUR SOQL QUERY INCLUDES A VARIABLE IN THE LIKE clause.. WE USE DATABASE.QUERY INSTEAD AND USE THE
		//	ASSEMBLED QUERY STRING
		String strAgencyName = saveAgencyName.replace('\'', '\\\'');	//we escape ' with \' before soql
		String query = 'SELECT Id, FirstName, LastName, Email, Profile.Name, Contact.Id, LastLoginDate, Account.Name, Account.Id, Contact.Agent_Code__c, Username ' +
					   'FROM User WHERE IsActive = true AND Account.Name = \'' + strAgencyName + '\' AND Id <> \'' + usrList[xrow].Id + '\'' +
					   'ORDER BY LastLoginDate ASC';
		System.Debug('query :>> ' + query);

		usrList = Database.query(query);
		System.Debug('Details usrList.size() :>> ' + usrList.size());

		//get the list of Agencies (Partner Accounts) for display -- used to transfer the agent to another Agency
		List<Account> accList = [SELECT Id, Name, OwnerId FROM Account WHERE Id !=: saveAcctId ORDER BY Name];
		System.Debug('accList.size() :>> ' + accList.size());
		AccName = null;
		if (accList.size() > 0) {
			for (Account ac : accList) {
				if (AccName != null) {
					AccName += '~' + ac.Name;
					AccRId  += '~' + ac.Id;
					AccOwn  += '~' + ac.OwnerId;
				}
				else {
					AccName = ac.Name;
					AccRId  = ac.Id;
					AccOwn  = ac.OwnerId;
				}
			}
		}
		System.Debug('AccName :>> ' + AccName);
	}

	public void updateAgentCode() {
		System.Debug('we are in updateAgentCode!');
		System.Debug('newData :>> ' + newData);
		System.Debug('cont.Id :>> ' + cont.Id);
		//simple lang naman.. eh di i-update ang Contact with new Agent Code
		cont.Agent_Code__c = newData;
		update cont;
	}

	public void updateAgentEmail() {
		System.Debug('we are in updateAgentEmail!');
		System.Debug('newData :>> 	 ' + newData);
		System.Debug('cont.Id :>> 	 ' + cont.Id);
		System.Debug('saveUserId :>> ' + saveUserId);
		//simple lang naman.. eh di i-update ang Contact and User with new Agent Email
		cont.Email = newData;
		update cont;

		User us = [SELECT Id, Username, Email FROM User WHERE Id =: saveUserId];
		// if (us.Username == us.Email)
		// 	us.Username = newData;
		us.Email = newData;
		update us;
	}

	public void resetPassword() {
		System.Debug('we are in resetPassword');
		System.Debug('saveUserId :>> ' + saveUserId);
		//isa pang simple lang hehe.. eh di i-reset ang password
		if (!Test.isRunningTest())
			System.resetPassword(saveUserId, true);
	}

	public void transferAgent() {
		System.Debug('we are in transferAgent!');
		rIndex--;		//we subtract 1 from index bec we added an option (select element -- Choose a new agency...)
		System.Debug('rIndex :>> ' + rIndex);
		//we convert the concatenation of Agency data to arrays
		List<String> arrNam = AccName.split('~');
		List<String> arrRId = AccRId.split('~');
		List<String> arrOwn = AccOwn.split('~');

		System.Debug('arrNam[rIndex] :>> ' + arrNam[rIndex]);	//for system.debug display only--arrNam can be removed
		System.Debug('cont.FirstName :>> ' + cont.FirstName);
		System.Debug('cont.LastName  :>> ' + cont.LastName) ;
		System.Debug('cont.Email     :>> ' + cont.Email);

		//get Agent Code used by agents of the new agency
		List<Contact> clist = [SELECT Agent_Code__c FROM Contact WHERE AccountId =: arrRId[rIndex]];
		if (clist.size() > 0)
			//get the Agent Code of the first record in the list
			cont.Agent_Code__c = clist[0].Agent_Code__c;
		else
			//if Agency has no agents yet, use 99999999 to alarm the admin user to change the code later
			cont.Agent_Code__c = '99999999';
		//this will in effect, transfer the Agent to the new Agency
		cont.AccountId = arrRId[rIndex];
		cont.OwnerId   = arrOwn[rIndex];
		update cont;

		//but we need to delete the old Account-Contact relationship (SF allows multiple relationships)
		//	THE TRANSFER ABOVE WILL NOT AUTOMATICALLY REMOVE THE CONTACT FROM ITS ORIGINAL ACCOUNT(AGENCY)
		delete [SELECT Id FROM AccountContactRelation WHERE ContactId =: cont.Id AND AccountId =: saveAcctId];

		showUserDetails = false;
		showSuccessTran = true;
	}

	public void deactivateUser() {
		System.Debug('we are in deactivateUser!');
		System.Debug('saveUserId :>> ' + saveUserId);
		System.Debug('cont.Contact.Id :>> ' + cont.Id);
		//we set isPortalEnabled to false to deactivate the user and set the ContactId to null (system does this)
		User udeact = [SELECT Id, Username FROM User WHERE Id =: saveUserId];
		udeact.Username = 'dx_' + udeact.Username;	//we add a prefix of dx_ to denote deactivation from this utility
		udeact.isPortalEnabled = false;				// system adds another _ before the dx_ when deactivating a user
		update udeact;

		//then we call our FutureHelper to delete the related Contact record -- BAWAL DAW DITO EH SABI SF HAHA
		zzFutureHelper.deleteContact(cont.Id);

		showUserDetails = false;
		showSuccessDeac = true;
	}

	public void sendUserEmail() {
		System.Debug('we are in sendVerifyEmail!');
		System.Debug('EmailTp :>> ' + EmailTp);
		System.Debug('EmailTo :>> ' + EmailTo);
		System.Debug('EmailCC :>> ' + EmailCC);

		Agent_Inquiry_Log__c ailog = new Agent_Inquiry_Log__c(
			Email_Type__c = EmailTp,
			Email_To__c	  = EmailTo,
			Email_CC__c	  = EmailCC
		);
		insert ailog;
	}

	public void xCancel() {
		System.Debug('we are in xCancel!');
		//PageReference pR;
		//we are just doing what our controller did in its constructor but we are also showing here
		//	a PageReference type of method instead of void -- needs a return at the end
		//	a return null would suffice.. the return pR is used if we intend to show another VFPage
		showPromptAgent = true;
		showSuccessTran = false;
		showSuccessDeac = false;
		showAgencyUList = false;
		showUserDetails = false;
		AgentEmail = '';

		//pR = ApexPages.currentPage();
		//return pR;
		//return null;
	}

	public void testFutureHelper() {
		zzFutureHelper.UpdateUser(saveUserId, 'New');
	}
}