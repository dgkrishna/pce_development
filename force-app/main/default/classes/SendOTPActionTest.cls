@isTest
public class SendOTPActionTest {
	
    //Normal Callout Test
    @isTest static void testNormalCallout(){
        
		Test.setMock(HttpCalloutMock.class, new SendOTPAPISuccessCalloutMockImpl());
        
        SendOTPAction.RequestOTPInput roi = new SendOTPAction.RequestOTPInput();
        roi.otpReceiptentContact='anonymous@test.com';
        roi.otpReceiptentReferenceId='A12345678';
        roi.otpSettingProfile='Smart_eMail';
        roi.templateParam=new List<String> {'Test','5 minutes'};
        roi.dummyMode=false;
        
        Test.startTest();
        List<SendOTPAction.RequestOTPOutput> roo = SendOTPAction.sendOTP(new List<SendOTPAction.RequestOTPInput>{roi});
        System.assert(roo!=null);
        System.assert(roo.size()>0);
        System.assertEquals('1000', roo[0].statusCode);
        Test.stopTest();
    }
    
    //Token Fail Test
    @isTest static void testTokenFail(){
        
		Test.setMock(HttpCalloutMock.class, new OTPTokenFailCalloutMockImpl());
        
        SendOTPAction.RequestOTPInput roi = new SendOTPAction.RequestOTPInput();
        roi.otpReceiptentContact='anonymous@test.com';
        roi.otpReceiptentReferenceId='A12345678';
        roi.otpSettingProfile='Smart_eMail';
        roi.templateParam=new List<String> {'Test','5 minutes'};
        roi.dummyMode=false;
        
        Test.startTest();
        List<SendOTPAction.RequestOTPOutput> roo = SendOTPAction.sendOTP(new List<SendOTPAction.RequestOTPInput>{roi});
        System.assert(roo!=null);
        System.assert(roo.size()>0);
        System.assertEquals('E22', roo[0].statusCode);
        Test.stopTest();
    }
    
    //OTP Fail Test
    @isTest static void testOTPFail(){
        
		Test.setMock(HttpCalloutMock.class, new SendOTPFailCalloutMockImpl());
        
        SendOTPAction.RequestOTPInput roi = new SendOTPAction.RequestOTPInput();
        roi.otpReceiptentContact='anonymous@test.com';
        roi.otpReceiptentReferenceId='A12345678';
        roi.otpSettingProfile='Smart_eMail';
        roi.templateParam=new List<String> {'Test','5 minutes'};
        roi.dummyMode=false;
        
        Test.startTest();
        List<SendOTPAction.RequestOTPOutput> roo = SendOTPAction.sendOTP(new List<SendOTPAction.RequestOTPInput>{roi});
        System.assert(roo!=null);
        System.assert(roo.size()>0);
        System.assertEquals('E16', roo[0].statusCode);
        Test.stopTest();
    }
}