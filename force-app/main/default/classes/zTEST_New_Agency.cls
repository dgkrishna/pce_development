@isTest
public class zTEST_New_Agency {

    @isTest static void testMyController() {
		Agency_Upload__c au = new Agency_Upload__c(
			Account_Name__c = 'The New Kid In Town',
			Billing_City__c = 'New Hampshire',
			Phone__c		= '8842-9090',
			TSM_Assigned__c = 'Marc Louis Cancio'
		);
		insert au;

		au = new Agency_Upload__c();
		au.Account_Name__c = 'Hotel California';
		au.Billing_City__c = 'Toronto, Canada';
		au.Phone__c 	   = '9463736563';
		au.TSM_Assigned__c = 'Marc Louis Cancio';
		insert au;

		Profile prof = [Select Id from Profile where name = 'SMART Territory Sales Manager User Profile'];
		User usr = new User(
			ProfileId = prof.Id,
			Username = System.now().millisecond() + 'test2@test.com',
			Alias = 'batman',
			Email='bruce.wayne@wayneenterprises.com',
			EmailEncodingKey='UTF-8',
			Firstname='Bruce',
			Lastname='Wayne',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='Asia/Manila'
		);
		insert usr;

		Test.StartTest();
			String TSMI = usr.Id + '~' + usr.Id;

			zzNew_Agency controller = new zzNew_Agency();

			controller.showUploadCSV();

			controller.gobackToMain();

			Blob bodyBlob = Blob.valueOf('Wrong Header\n');
			controller.csvFileBody = bodyBlob;
			controller.importCSVFile();

			bodyBlob = Blob.valueOf('Account Name,Billing City,Phone,TSM Assigned\nTest Agency Name,Makati,09191919111,TSM One');
			controller.csvFileBody = bodyBlob;
			controller.importCSVFile();

			controller.createRecords();

			controller.AgencyTSMI = TSMI;
			controller.createAgency();
        Test.StopTest();
    }
}