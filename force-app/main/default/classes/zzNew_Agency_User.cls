public class zzNew_Agency_User {
	//GET/SET variables
		public String divHeight {get; set;}

		public String AgencyName {get; set;}
		public String UserFName {get; set;}
		public String UserLName {get; set;}
		public String UserEmail {get; set;}
		public String UserPhone {get; set;}
		public String UserPRole {get; set;}
		public String UserACode {get; set;}
		public String UserIsInc {get; set;}

		public String UserUpInc {get; set;}

		public Blob csvFileBody	  {get;set;}
		public String csvAsString {get;set;}
		public List<Agency_User_Upload__c> agulist {get;set;}

		public Boolean showSPromptScr {get; set;}
		public Boolean showFUploadScr {get; set;}
		public Boolean showResltScr {get; set;}
		public Boolean showTableScr {get; set;}

		public String  resultMessageE {get; set;}
		public String  resultMessageS {get; set;}

		public String UserRecId;
		public Boolean recImport;

		public List<Agency_User_Upload__c> agup;	//this is in preparation for when we activate the Update Tags feature
		public zzFutureHelper helper;

	public zzNew_Agency_User() {
		list<User> userlist = [SELECT Id
								FROM User
								WHERE IsActive = true AND Profile.Name = 'SMART Territory Sales Manager User Profile'
								ORDER BY Name ASC];
		//special processing lang to ha! para lang maayos ko yong div wrapper ng table display hehehe
		if (userlist.size() > 40)
			divHeight = '567';		//Prod
		else
			divHeight = '537';		//UAT

		getNewValues();

		showSPromptScr = true;
	}

	public void getNewValues() {
		AgencyName = null;
		UserFName = null;
		UserLName = null;
		UserEmail = null;
		UserPhone = null;
		UserPRole = null;
		UserACode = null;
		UserIsInc = null;
		UserRecId = null;

		agup = [SELECT Id, Account_Name__c, First_Name__c, Last_Name__c, Email__c, Phone__c, Agent_Code__c, Role__c, Is_Included__c
					FROM Agency_User_Upload__c
					WHERE Status__c = false
					ORDER BY Account_Name__c ASC, Role__c DESC];

		//MAKE SURE THE ORDER BY Account_Name__c is kept always because the processing below assumes that the
		//	account numbers are ordered
		//we convert our result object to individual strings of '~' delimited data to be passed on to our VFP
		if (agup.size() > 0) {
			for (Agency_User_Upload__c au: agup) {
				if (AgencyName != null) {
					AgencyName += '~' + au.Account_Name__c;
					UserFName += '~' + au.First_Name__c;
					UserLName += '~' + au.Last_Name__c;
					UserEmail += '~' + au.Email__c;
					UserPhone += '~' + au.Phone__c;
					UserPRole += '~' + au.Role__c;
					UserACode += '~' + au.Agent_Code__c;
					if (au.Is_Included__c == true)
						UserIsInc += '~' + 'Yes';
					else
						UserIsInc += '~' + 'No';
					UserRecId += '~' + au.Id;
				}
				else {
					AgencyName = au.Account_Name__c;
					UserFName = au.First_Name__c;
					UserLName = au.Last_Name__c;
					UserEmail = au.Email__c;
					UserPhone = au.Phone__c;
					UserPRole = au.Role__c;
					UserACode = au.Agent_Code__c;
					if (au.Is_Included__c == true)
						UserIsInc = 'Yes';
					else
						UserIsInc = 'No';
					UserRecId = au.Id;
				}
			}

			recImport = false;	//everytime we got something from the New Device upload file we set this to false
								//so that when the back button is clicked.. there will be no unnecessary getting of records again
		}
	}

	public void showUploadCSV() {
		showSPromptScr = false;
		showFUploadScr = true;
	}

	public void gobackToMain() {
		//if our success Import flag is set to true.. we read New Devices object again
		if (recImport == true)
			getNewValues();

		showFUploadScr = false;
		showTableScr   = false;
		showResltScr   = false;
		showSPromptScr = true;
	}

	public void importCSVFile() {
		String[] csvFileLines = new String[] {};
		agulist = New List<Agency_User_Upload__c>();

		csvAsString  = csvFileBody.toString();
		csvFileLines = csvAsString.split('\n');

		String[] csvRecordData = csvFileLines[0].split(',');
		if (csvRecordData[0].trim() == 'Email'
				&& csvRecordData[1].trim() == 'First Name'
				&& csvRecordData[2].trim() == 'Last Name'
				&& csvRecordData[3].trim() == 'Phone'
				&& csvRecordData[4].trim() == 'Account Name'
				&& csvRecordData[5].trim() == 'Role'
				&& csvRecordData[6].trim() == 'Agent Code') {
			for (Integer i=1; i<csvFileLines.size(); i++) {
				Agency_User_Upload__c aguObj = new Agency_User_Upload__c() ;
				csvRecordData = csvFileLines[i].split(',');
					aguObj.Email__c = csvRecordData[0].trim();
					aguObj.First_Name__c   = csvRecordData[1].trim();
					aguObj.Last_Name__c    = csvRecordData[2].trim();
					aguObj.Phone__c 	   = csvRecordData[3].trim();
					aguObj.Account_Name__c = csvRecordData[4].trim();
					aguObj.Role__c  	   = csvRecordData[5].trim();
					aguObj.Agent_Code__c   = csvRecordData[6].trim();
				//add record to List
				agulist.add(aguObj);
			}

			showResltScr = false;
			showTableScr = true;
		}
		else {
			System.Debug('Maybe wrong file - Error in CSV Header');
			showTableScr = false;
			showResltScr = true;
			resultMessageE = 'You may have uploaded the wrong file. The CSV column header/s is/are not as expected!';
			resultMessageS = '';
		}
	}

	public void createRecords() {
		insert agulist;

		showTableScr = false;
		showResltScr = true;
		resultMessageS = 'New Agency User records were successfully imported from CSV data! Ready to create New Agency Users.. ' +
			'Please click Back button now to create New Agency Users from Main Screen.';
		resultMessageE = '';
		recImport = true;	//everytime we successfully import records from CSV to New Devices object, we set this to true
	}

	public void createAgencyUsers() {
		//public Pagereference createAgencyUsers() {
		System.Debug('we are in createAgencyUsers!');
		System.Debug('UserUpInc :>> '  + UserUpInc);

		//convert all string concatenations to array for processing
			List<String> lstANames = AgencyName.split('~');
			List<String> lstFNames = UserFName.split('~');
			List<String> lstLNames = UserLName.split('~');
			List<String> lstEmails = UserEmail.split('~');
			List<String> lstPhones = UserPhone.split('~');
			List<String> lstPRoles = UserPRole.split('~');
			List<String> lstACodes = UserACode.split('~');
			List<String> lstIsIncs = UserIsInc.split('~');		//this is the original inclusion list
			List<String> lstUpIncs = UserUpInc.split('~');		//this is our new inclusion list from vfpage

			List<String> lstRecIds = UserRecId.split('~');
			System.Debug('lstANames.size() :>> ' + lstANames.size());

		//this part gets the Profile Ids of both agency Manager and User
			List<String> ProfId = new List<String>{'',''};
			Integer num = 0;
			//get Profile IDs of Agency Manager and Agency User
			List<Profile> prof = [SELECT Id, Name FROM Profile WHERE Name = 'SMART Agency Manager' OR Name = 'SMART Agency User' ORDER BY Name ASC];
			for (Profile pr : prof) {
				ProfId[num] = pr.Id;
				num++;
			}

		//these are our working variables
			String saveAName = '';
			Id saveAccountId;
			Id saveAcOwnerId;
			Id saveProfileId;
			Boolean IsError;

		//AND HERE.. WE ITERATE THRU OUR UPLOADED FILE TO CREATE AGENCY USERS********
			for (Integer i=0; i<lstANames.size(); i++) {
				//we access the uploaded record, BY ID, for the data currently being processed.. ready for update
					Agency_User_Upload__c auu = [SELECT Id FROM Agency_User_Upload__c WHERE Id =: lstRecIds[i]];
					if (lstUpIncs[i] == 'Yes')
						auu.Is_Included__c = true;
					else
						auu.Is_Included__c = false;

				//this is where we ask if the current row in our array is to be processed or NOT!
				//based on the inclusion list from our VFPage
				if (lstUpIncs[i] == 'Yes') {
					System.Debug('Yes for this User: ' + lstFNames[i] + ' ' + lstLNames[i]);
					System.Debug('Start Processing for Index ' + (i + 1));

					//if our Account Name has changed from last one -- our processing is sorted by Account Name
					//we access the account record to get the Account Id to link our new Contact to!!!
					if (lstANames[i] != saveAName) {
						list<Account> acc = [SELECT Id, OwnerId FROM Account WHERE Name =: lstANames[i]];
						if (acc.size() > 0) {
							saveAccountId = acc[0].Id;
							System.Debug('saveAccountId :>> ' + saveAccountId);
							saveAcOwnerId = acc[0].OwnerId;
							IsError = false;
						}
						else {
							saveAccountId = null;
							saveAcOwnerId = null;
							IsError = true;
						}

						saveAName = lstANames[i];
					}

					if (!IsError) {
						//then we create our Contact -- linked to the Account Record
							Contact cont = new Contact(
								AccountId = saveAccountId,
								Email 	  = lstEmails[i],
								FirstName = lstFNames[i],
								LastName  = lstLNames[i],
								Phone 	  = lstPhones[i],
								MobilePhone   = lstPhones[i],
								Agent_Code__c = lstACodes[i],
								Agent_TSM__c  = saveAcOwnerId,
								OwnerId 	  = saveAcOwnerId
							);

							insert cont;

						//we obtain the correct Profile based on the Role that is in the source file
							if (lstPRoles[i].contains('Manager'))
								saveProfileId = ProfId[0];
							else
								saveProfileId = ProfId[1];

						//then we create our User that is linked to the above newly created Contact (indirectly Enabling Partner User)
							Integer subslen;
							if (lstLNames[i].length() < 4)
								subslen = lstLNames[i].length();
							else
								subslen = 4;

							User usr = new User(
								ProfileId = saveProfileId,
								ContactId = cont.Id,
								FirstName = lstFNames[i],
								LastName  = lstLNames[i],
								Username  = lstEmails[i],
								Email = lstEmails[i],
								Alias = lstFNames[i].substring(0, 1).toLowercase() + lstLNames[i].substring(0, subslen).toLowercase(),
								CommunityNickname = lstFNames[i] + lstLNames[i].substring(0, 1),
								EmailEncodingKey  = 'UTF-8',
								TimeZoneSidKey 	  = 'Asia/Manila',
								LocaleSidKey 	  = 'en_US',
								LanguageLocaleKey = 'en_US',
								UserPermissionsMarketingUser = true,
								IsActive = true
							);
							Database.insert(usr);

						//if the agency user is a manager.. we have to change the role because the default role is agency user/agent
						//	we use another controller class with @future because sequential update from creation is not allowed here
						//	for setup and non-setup objects
						if (lstPRoles[i].contains('Manager')) {
							zzFutureHelper.UpdateUser(usr.Id, lstANames[i]);
						}
						//if successful.. we update the Status of the upload record to true so it won't show next access to VFPage
						auu.Error__c  = '';
						auu.Status__c = true;
						auu.Status_Date__c = Datetime.now();
					}
					else {
						auu.Error__c = 'Invalid Account Name';
						auu.Is_Included__c = false;
					}

					System.Debug('End Processing for Index ' + (i + 1));	//we put this here so that if an exception occurs and with all
					//or nothing happening, we would know which record caused the exception and can exclude it in the next attempt
				}

				update auu;
			}

		showSPromptScr = false;
		showResltScr = true;
		resultMessageS = 'New Agency Users successfully created! You can go to Contacts/Users and check their existence there.';
		resultMessageE = '';
	}
}