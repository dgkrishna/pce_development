@isTest
public class ResendOTPActionTest {
    //Normal Callout Test
    @isTest static void testNormalCallout(){
        
		Test.setMock(HttpCalloutMock.class, new ResendOTPAPISuccessCalloutMockImpl());
        
        ResendOTPAction.ResendOTPInput roi = new ResendOTPAction.ResendOTPInput();
        roi.otpReceiptentContact='anonymous@test.com';
        roi.otpReceiptentReferenceId='A12345678';
        roi.otpSettingProfile='Smart_eMail';
        roi.templateParam=new List<String> {'Test','5 minutes'};
        roi.dummyMode=false;
        
        Test.startTest();
        List<ResendOTPAction.ResendOTPOutput>roo = ResendOTPAction.resendOTP(new List<ResendOTPAction.ResendOTPInput>{roi});
        System.assert(roo!=null);
        System.assert(roo.size()>0);
        System.assertEquals('3000', roo[0].statusCode);
        Test.stopTest();
    }
    
    //OTP Fail Test
    @isTest static void testOTPFail(){
        
		Test.setMock(HttpCalloutMock.class, new ResendOTPAPIFailCalloutMockImpl());
        
        ResendOTPAction.ResendOTPInput roi = new ResendOTPAction.ResendOTPInput();
        roi.otpReceiptentContact='anonymous@test.com';
        roi.otpReceiptentReferenceId='A12345678';
        roi.otpSettingProfile='Smart_eMail';
        roi.templateParam=new List<String> {'Test','5 minutes'};
        roi.dummyMode=false;
        
        Test.startTest();
        List<ResendOTPAction.ResendOTPOutput>roo = ResendOTPAction.resendOTP(new List<ResendOTPAction.ResendOTPInput>{roi});
        System.assert(roo!=null);
        System.assert(roo.size()>0);
        System.debug('ResendOTPActionTest.testOTPFail : ' + roo[0].statusCode);
        System.assertEquals('E18', roo[0].statusCode);
        Test.stopTest();
    }
}