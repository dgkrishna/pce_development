/*************************************************************************************************************
* @name            ReendOTPAction
* @author          Albert Poon <albert.poon@salesforce.com>
* @created         06 / 06 / 2021
* @description     Provide function to resend OTP  
* 	
*
* Changes (version)
* -----------------------------------------------------------------------------------------------------------
*              No.     Date            Author                  Description
*              ----    ------------    --------------------    ----------------------------------------------
* @version     1.0     2021-06-06      Albert Poon			   initial version
* @version     2.0     2021-08-23      Albert Poon			   implement auto retry
*
**************************************************************************************************************/
global class ResendOTPAction {
    global class ResendOTPInput {
        @InvocableVariable(label='OTP Setting Profile' description='Relative metadata type profile API Name of Cloudsales_OTP_Setting for this OTP request.' required=true)
        global String otpSettingProfile;
        @InvocableVariable(label='Customer Reference Number' description='Reference Number that belongs to customer who triggered for OTP. Expect to use record ID of SF' required=true)
        global String otpReceiptentReferenceId;
        @InvocableVariable(label='OTP Receiptent Contact' description='The receipent contact, e.g. eMail address or mobile number' required=true)
        global String otpReceiptentContact;
        @InvocableVariable(label='Template Parameters' description='The parameters to populate the template' required=true)
        global List<String> templateParam;
        @InvocableVariable(label='Dummy Mode' description='Turn on dummy mode for OTP (API call always success)' required=true)
        global boolean dummyMode;
        
        global ResendOTPInput() {
        }
    }
    
    global class ResendOTPOutput {
        @InvocableVariable(label='StatusCode' description='Status Code')
        global String statusCode;
        @InvocableVariable(label='StatusMessage' description='Status Message')
        global String statusMessage;
        global ResendOTPOutput(String inStatusCode, String inStatusMessage) {
            statusCode=inStatusCode;
            statusMessage=inStatusMessage;
        }
    }
    
    
    /* Resend OTP to user to be used by Flow
     * @param  resendOTPInput the OTP setting name in Cloudsales OTP metadata type
     * @return 2 Strings will be returned: 
     * 		   1)Status code
     * 		   2)Status Message
     */
    @InvocableMethod
    public static List<ResendOTPOutput> resendOTP(List<ResendOTPInput> resendOTPInput) {
        List<ResendOTPOutput> out=new List<ResendOTPOutput>{};
            if(resendOTPInput.size()>0) {
                
                Cloudsales_OTP_Setting__mdt[] OTPSettingMapping = [SELECT OTP_Charset__c, OTP_Digits__c, OTP_Credential_Name__c,
                                                                   Resend_OTP_URL_Path__c, OTP_Type__c, OTP_Validity__c,
                                                                   Request_Type_ID__c, Template_ID__c, URL_Path_Base__c,
                                                                   Connection_Retry__c
                                                                   FROM Cloudsales_OTP_Setting__mdt
                                                                   WHERE QualifiedApiName=:resendOTPInput[0].otpSettingProfile];
                
                //If OTP Setting metadata type of specific profile is found
                if(OTPSettingMapping!=null && OTPSettingMapping.size()>0) {
                    Cloudsales_OTP_Auth_Credential__mdt[] OTPCredentialConfig = [SELECT Client_Id__c, Client_Secret__c,Label, Token_Endpoint__c
                                                                                 FROM Cloudsales_OTP_Auth_Credential__mdt
                                                                                 WHERE Label=:OTPSettingMapping[0].OTP_Credential_Name__c
                                                                                 LIMIT 1];
                    //If OTP Auth Credential metadata type is found
                    if(OTPCredentialConfig!=null && OTPCredentialConfig.size()>0) {
                        Integer retryCount = 1;
                        String token=null;
                        
                        do{
                            //Acquire asccess token
                            token = SmartOTPTokenAcquireUtil.retrieveToken(OTPCredentialConfig[0]);
                            retryCount++;
                        } while((token==null || token.length()<=0) && retryCount<=OTPSettingMapping[0].Connection_Retry__c);
                        
                        if(token!=null && token.length()>0) {
                            Http http = new Http();
                            HttpRequest request = new HttpRequest();
                            
                            request.setEndpoint(OTPSettingMapping[0].URL_Path_Base__c+OTPSettingMapping[0].Resend_OTP_URL_Path__c);
                            
                            //Preparing body
                            Map<String, Object> obj = new Map<String, Object>();
                            obj.put('otpDigits', OTPSettingMapping[0].OTP_Digits__c);
                            obj.put('otpCharset', OTPSettingMapping[0].OTP_Charset__c);
                            obj.put('otpValidity', OTPSettingMapping[0].OTP_Validity__c);
                            obj.put('requestTypeId', OTPSettingMapping[0].Request_Type_ID__c);
                            obj.put('requestIdentifier', resendOTPInput[0].otpReceiptentReferenceId);
                            
                            List<Map<String, Object>> channels = new List<Map<String, Object>>();
                            Map<String, Object> channel = new Map<String, Object>();
                            channel.put('type', OTPSettingMapping[0].OTP_Type__c);
                            channel.put('targetId', resendOTPInput[0].otpReceiptentContact);
                            channel.put('templateId', OTPSettingMapping[0].Template_ID__c);
                            
                            resendOTPInput[0].templateParam.add(Integer.valueOf(OTPSettingMapping[0].OTP_Validity__c)/60+' minutes');
                            channel.put('templateParams', resendOTPInput[0].templateParam);
                            
                            channels.add(channel);
                            obj.put('channels', channels);
                            obj.put('generateNew', 'true');
                            System.debug('resendOTP Request : ' + JSON.Serialize(obj));
                            
                            request.setHeader('Content-Length', String.valueOf(JSON.Serialize(obj).length()));
                            request.setHeader('Content-Type', 'application/json');
                            request.setHeader('Connection', 'keep-alive');
                            request.setHeader('Authorization', 'Bearer '+token);
                            request.setTimeout(10000);
                            
                            request.setBody(JSON.Serialize(obj));
                            request.setMethod('POST');
                            
                            String responseCode;
                            HttpResponse response;
                            retryCount=1;
                            
                            try {
                                do {
                                    responseCode='';
                                    
                                    response = http.send(request);
                                    if(response.getStatusCode()==200){
                                        System.debug('resendOTP Response RAW : ' + response.getBody());
                                        
                                        //process response
                                        List<Object> responseJson = (List<Object>)JSON.deserializeUntyped(response.getBody());
                                        
                                        System.debug('resendOTP Response : ' + responseJson[0]);
                                        responseCode=(String)((Map<String, Object>)responseJson[0]).get('statusCode');
                                        
                                        if(responseCode=='3000' || responseCode=='9003' || responseCode=='9004' ||
                                           responseCode=='9005' ||responseCode=='9007' || responseCode=='9008') {
                                               out.add(new ResendOTPOutput((String)((Map<String, Object>)responseJson[0]).get('statusCode'),
                                                                           (String)((Map<String, Object>)responseJson[0]).get('statusMessage')));
                                           }
                                    }
                                    retryCount++;
                                    
                                } while(response.getStatusCode()!=200 && retryCount<=OTPSettingMapping[0].Connection_Retry__c && 
                                        responseCode!='3000' && responseCode!='9003' && responseCode!='9004' && 
                                        responseCode!='9005' && responseCode!='9007' && responseCode!='9008');
                                
                                if((response!=null && response.getStatusCode()!=200) || (responseCode!='3000' && responseCode!='9003' && responseCode!='9004' &&
                                   responseCode!='9005' && responseCode!='9007' && responseCode!='9008')) {
                                       System.debug('E18 - Unable to connect to OTP API');
                                       out.add(new ResendOTPOutput('E18','Unable to connect to OTP API'));
                                   }
                                
                            } catch(System.CalloutException ex) {
                                System.debug(ex.getMessage());
                                out.add(new ResendOTPOutput('E18','Unable to connect to OTP API'));
                            }
                            
                            
                        } else {
                            System.debug('E24 - Unable to acquire token for OTP API');
                            out.add(new ResendOTPOutput('E24','Configuration profile "' + OTPSettingMapping[0].OTP_Credential_Name__c+'" not found'));
                        }
                    } else {
                        System.debug('E21 - Configuration profile "' + OTPSettingMapping[0].Send_OTP_URL_Path__c+'" not found');
                        out.add(new ResendOTPOutput('E21','Configuration profile "' + OTPSettingMapping[0].Send_OTP_URL_Path__c+'" not found'));
                    }
                } else {
                    System.debug('E05 - Configuration profile "' + resendOTPInput[0].otpSettingProfile+'" not found');
                    out.add(new ResendOTPOutput('E05','Configuration profile "' + resendOTPInput[0].otpSettingProfile+'" not found'));
                }
            } else {
                System.debug('E06 - Empty request is provided');
                out.add(new ResendOTPOutput('E06','Empty request is provided'));
            }
        return out;
    }
}