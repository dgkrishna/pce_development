global class VerifyOTPAPIFailCalloutMockImpl implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        if(req.getEndpoint().startsWith('https://stg01.smart.com.ph/digitalapigw-sit/usso-sit/connect/token')){
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json; charset=utf-8');
            res.setHeader('X-Content-Type-Options', 'nosniff');
            res.setHeader('X-Frame-Options', 'deny');
            res.setBody('{"access_token":"eyJhbGciOiJSUzI1NiIsImtpZCI6IkQ2N0EyMkM3MzQzN0IxMDY5RjUyMjkxMkUxNjA3ODNBNkUwQjMwNkMiLCJ0eXAiOiJhdCtqd3QiLCJ4NXQiOiIxbm9peHpRM3NRYWZVaWtTNFdCNE9tNExNR3cifQ.eyJuYmYiOjE2MzA1NjQyODksImV4cCI6MTYzMDU2Nzg4OSwiaXNzIjoiaHR0cHM6Ly9zdGctd3MuaW50cmEuc21hcnQvdXNzby1zaXQiLCJhdWQiOlsiYXBpZ2F0ZXdheSIsIm90cCIsIm90cF9ub3RpZmljYXRpb25zIl0sImNsaWVudF9pZCI6IkNsb3Vkc2FsZXMtQ0NGIiwic2NvcGUiOlsiYXBpZ2F0ZXdheSIsIm90cCIsIm90cF9ub3RpZmljYXRpb25zIl19.LdRlUM0SAEagIiKbXNMOdgICvkEjix0MRZCnGXkZAM5YaF9OpiYDbYOPpMLACSUau8aefmM1-Dvme-9P_CoOFPZxZSsc3gqTyOXuBz0JDlRXoF1dDoN6o-yHj8nOhebfgAS9H6ULK1XdkgyEdaY04_plbjh1CVxsOwAbIJjemaZQ6T214s11QxDmZ2stKb-YDmWKQ1XYxLmpTQ9XszUg1UMyo5Z0e3MU_77kGGgpM0OKua8L9hFNVweRzDbwyXuKmLTJ5MD0AHTEr7yo_GrkO3GxiD1KY3AU_jAPh9AkJnXo8M4jJSCOs_Lf-ZCqfy3ldJ5gpIoiLplanx761OaCyRhTda1xMG13GqoPgSKDDejwm0f8DX36ONb0iKYsTvdKgpqEMyB6Uauo3kysctfcznjJ6fVXu7ZEeePmM95WC29nyNK8yC9X6qBQ9O83GyStMmtH8dldqmhQ6beHJr67IsywpbrKoWQrX2PrtPJePOmTG11aMMbnGBIRC_QEeLj_Kjq_Aa8YNu1W5ldx9KrLjXYJ_UQyI-OxHlUsAF6xjuDHxznv6neMdqmyVVQzNbmjYmVOVoaCcouzaXpdgrU6E7aoNdR2rkmAUfTKLYcCIA7z3h7SeVIa9Txjz_QVTA-yfO8cmnboRIFmrFiTtmLLiP6YWLuJE8fAA7P8UdnSzME","expires_in":3600,"token_type":"Bearer","scope":"apigateway openid otp otp_notifications"}');
            res.setStatusCode(200);
            return res;
        } else {        
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            System.assertEquals('POST', req.getMethod());
            System.assert(req.getHeader('Authorization')!=null);
            //Verify request
            Map<String, Object> reqJson = (Map<String, Object>) JSON.deserializeUntyped(req.getBody());
            System.assert(reqJson.get('otpCode')!=null);
            System.assert(reqJson.get('requestTypeId')!=null);
            System.assert(reqJson.get('requestIdentifier')!=null);
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json; charset=utf-8');
            res.setHeader('X-Content-Type-Options', 'nosniff');
            res.setHeader('X-Frame-Options', 'deny');
            res.setBody('{"statusCode": "9001","statusMessage": "The OTP validated successfully.","trxId": "17956"}');
            res.setStatusCode(200);
            return res;
        }
    }
}