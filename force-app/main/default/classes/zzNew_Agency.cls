public class zzNew_Agency {
	//GET/SET variables
		public String divHeight {get; set;}

		public String TSMNames {get; set;}
		public String TSMUsrId {get; set;}

		public String AgencyName {get; set;}
		public String AgencyCity {get; set;}
		public String AgencyPhne {get; set;}
		public String AgencyTSMU {get; set;}

		public String AgencyTSMI {get; set;}

		public Blob csvFileBody	  {get;set;}
		public String csvAsString {get;set;}
		public List<Agency_Upload__c> agnlist {get;set;}

		public Boolean showSPromptScr {get; set;}
		public Boolean showFUploadScr {get; set;}
		public Boolean showResltScr {get; set;}
		public Boolean showTableScr {get; set;}

		public String  resultMessageE {get; set;}
		public String  resultMessageS {get; set;}

		public String UserRecId;
		public Boolean recImport;

	public zzNew_Agency() {
		//we select all TSMs from the User object to pass to VFP for selection and assignment
		list<User> userlist = [SELECT Id, Name
								FROM User
								WHERE IsActive = true AND Profile.Name = 'SMART Territory Sales Manager User Profile'
								ORDER BY Name ASC];
		//special processing lang to ha! para lang maayos ko yong div wrapper ng table display hehehe
		if (userlist.size() > 40)
			divHeight = '450';		//Prod
		else
			divHeight = '420';		//UAT

		for (User user : userlist){
			if (TSMNames != null) {
				TSMNames += '~' + user.Name;
				TSMUsrId += '~' + user.Id;
			}
			else {
				TSMNames = user.Name;
				TSMUsrId = user.Id;
			}
		}

		getNewValues();

		showSPromptScr = true;
	}

	public void getNewValues() {
		AgencyName = null;
		AgencyCity = null;
		AgencyPhne = null;
		AgencyTSMU = null;
		UserRecId  = null;

		//then we select all New Agency uploaded records with Status = false for processing
		list<Agency_Upload__c> agup = [SELECT Account_Name__c, Billing_City__c, Phone__c, TSM_Assigned__c
										FROM Agency_Upload__c
										WHERE Status__c = false
										ORDER BY TSM_Assigned__c, Account_Name__c];
		if (agup.size() > 0) {
			for (Agency_Upload__c au: agup) {
				if (AgencyName != null) {
					AgencyName += '~' + au.Account_Name__c;
					AgencyCity += '~' + au.Billing_City__c;
					AgencyPhne += '~' + au.Phone__c;
					AgencyTSMU += '~' + au.TSM_Assigned__c;
					UserRecId  += '~' + au.Id;		//we do this so that we will be able to update object later
				}
				else {
					AgencyName = au.Account_Name__c;
					AgencyCity = au.Billing_City__c;
					AgencyPhne = au.Phone__c;
					AgencyTSMU = au.TSM_Assigned__c;
					UserRecId  = au.Id;
				}
			}

			recImport = false;	//everytime we got something from the New Device upload file we set this to false
								//so that when the back button is clicked.. there will be no unnecessary getting of records again
		}
	}

	public void showUploadCSV() {
		showSPromptScr = false;
		showFUploadScr = true;
	}

	public void gobackToMain() {
		//if our success Import flag is set to true.. we read New Devices object again
		if (recImport == true)
			getNewValues();

		showFUploadScr = false;
		showTableScr   = false;
		showResltScr   = false;
		showSPromptScr = true;
	}

	public void importCSVFile() {
		String[] csvFileLines = new String[] {};
		agnlist = New List<Agency_Upload__c>();

		csvAsString  = csvFileBody.toString();
		csvFileLines = csvAsString.split('\n');

		String[] csvRecordData = csvFileLines[0].split(',');
		if (csvRecordData[0].trim() == 'Account Name'
				&& csvRecordData[1].trim() == 'Billing City'
				&& csvRecordData[2].trim() == 'Phone'
				&& csvRecordData[3].trim() == 'TSM Assigned') {
			for (Integer i=1; i<csvFileLines.size(); i++) {
				Agency_Upload__c agnObj = new Agency_Upload__c() ;
				csvRecordData = csvFileLines[i].split(',');
					agnObj.Account_Name__c = csvRecordData[0].trim();
					agnObj.Billing_City__c = csvRecordData[1].trim();
					agnObj.Phone__c 	   = csvRecordData[2].trim();
					agnObj.TSM_Assigned__c = csvRecordData[3].trim();
				//add record to List
				agnlist.add(agnObj);
			}

			showResltScr = false;
			showTableScr = true;
		}
		else {
			System.Debug('Maybe wrong file - Error in CSV Header');
			showTableScr = false;
			showResltScr = true;
			resultMessageE = 'You may have uploaded the wrong file. The CSV column header/s is/are not as expected!';
			resultMessageS = '';
		}
	}

	public void createRecords() {
		insert agnlist;

		showTableScr = false;
		showResltScr = true;
		resultMessageS = 'New Agency records were successfully imported from CSV data! Ready to create New Agencies.. ' +
			'Please click Back button now to create New Agencies from Main Screen.';
		resultMessageE = '';
		recImport = true;	//everytime we successfully import records from CSV to New Devices object, we set this to true
	}

	public void createAgency() {
		System.Debug('we are in createAgency!');
			System.Debug('AgencyName :>> ' + AgencyName);
			System.Debug('AgencyCity :>> ' + AgencyCity);
			System.Debug('AgencyPhne :>> ' + AgencyPhne);
			System.Debug('AgencyTSMI :>> ' + AgencyTSMI);
		//we convert all String concatenations to apex arrays (lists)
			List<String> lstNames = AgencyName.split('~');
			List<String> lstCitys = AgencyCity.split('~');
			List<String> lstPhnes = AgencyPhne.split('~');
			List<String> lstTSMIs = AgencyTSMI.split('~');

			List<String> lstRecIds = UserRecId.split('~');

		System.Debug('lstNames.size() :>> ' + lstNames.size());	 //shows in debug how many rows were passed from VFP
		//this here is where we actually iterate through all the arrays to process rows for creation of new Agencies
		for (Integer i=0; i<lstNames.size(); i++) {
			//we access the uploaded record, BY ID, for the data currently being processed.. ready for update
			Agency_Upload__c au = [SELECT Id FROM Agency_Upload__c WHERE Id =: lstRecIds[i]];

			//we only process TSMI rows that have real TSM IDs.. if 'blank', we skip -- simple lang noh?
			if (lstTSMIs[i] != 'blank') {
				System.Debug('Agency Name: ' + lstNames[i]);
				System.Debug('Start Processing for Index ' + (i + 1));

				//we proceed to create the Partner Account for each row in the arrays processed
				Account agencyAccount = new Account(
					Name 		= lstNames[i],
					BillingCity = lstCitys[i],
					Phone 		= lstPhnes[i],
					OwnerId 	= lstTSMIs[i]
				);
				//OwnerId = String.valueOf(lstTSMIs[i]).substring(0, 15) - if 18 char Id doesn't WORK HEHEHE -- BUT IT WORKS!!!
				Database.insert(agencyAccount);

				//WE CANNOT CREATE THE ACCOUNT WITH THIS SET TO TRUE.. ONLY UPDATE IS ALLOWED.. SO WE UPDATE - PROBLEMA BA YAN!
				agencyAccount.IsPartner = true;
				update agencyAccount;
				//we update the Status field of the record affected so that next time we use this tool.. not display anymore
				au.Status__c = true;
				au.Status_Date__c = Datetime.now();
				update au;
				System.Debug('End Processing for Index ' + (i + 1));	//we put this here so that if an exception occurs and with all
				//or nothing happening, we would know which record caused the exception and can exclude it in the next attempt
			}
			else
				System.Debug('Index ' + (i + 1) + ' skipped!');
		}

		showSPromptScr = false;
		showResltScr = true;
		resultMessageS = 'New Agencies successfully created! You can go to Accounts and check their existence there.';
		resultMessageE = '';
	}
}