public class zzPFS_Statistics {
	//public variables
		public Map<String, Map<String, String>> liclist {get; set;}
		public Map<String, Map<String, String>> orglist {get; set;}
		public Map<String, Map<String, String>> parlist {get; set;}
		public Map<String, Map<String, String>> loglist {get; set;}
		public Map<String, Map<String, String>> agylist {get; set;}
		public Map<String, Map<String, String>> tstlist {get; set;}
		public String orgnumber {get; set;}
		public String parnumber {get; set;}
		public String agynumber {get; set;}

	public zzPFS_Statistics() {
		getStats();
	}

	public void getStats() {
		//Organization User License Information
			List<UserLicense> lics = [SELECT LicenseDefinitionKey, TotalLicenses, UsedLicenses
										FROM UserLicense
										WHERE Status = 'Active'];

			liclist = new Map<String, Map<String, String>>();
			Map<String, String> interMap;

			if (lics.size() > 0) {
				Integer row = 0;
				for (UserLicense obj : lics) {
					System.Debug('obj.LicenseDefinitionKey :>> ' + obj.LicenseDefinitionKey);
					interMap = new Map<String, String>();
					interMap.put('LicenseDefinitionKey', String.valueOf(obj.LicenseDefinitionKey));
					interMap.put('TotalLicenses', String.valueOf(obj.TotalLicenses));
					interMap.put('UsedLicenses',  String.valueOf(obj.UsedLicenses));
					Integer bal = Integer.valueOf(obj.TotalLicenses) - Integer.valueOf(obj.UsedLicenses);
					interMap.put('BalLicenses', String.valueOf(bal));
					System.Debug('interMap.size() :>> ' + interMap.size());
					liclist.put(String.valueOf(row), interMap);
					row++;
				}

				System.Debug('liclist.size() :>> ' + liclist.size());
			}

		//Organic User Distribution
			List<AggregateResult> result = [SELECT count(id), ProfileId
												FROM User
												WHERE (NOT Profile.Name LIKE '%SMART Agency%') and IsActive = true
												GROUP BY ProfileId];

			orglist = new Map<String, Map<String, String>>();
			List<Profile> pro = new List<Profile>();
			String pId;
			String strname;
			String stcount;
			Integer row = 0;
			Integer incount = 0;
			for (AggregateResult ar : result)  {
				pId = String.valueOf(ar.get('ProfileId'));
				pro = [SELECT Name FROM Profile WHERE Id =: pId AND UserLicense.Name = 'Salesforce'];
				if (pro.size() > 0) {
					strname = pro[0].Name;
					stcount = String.valueOf(ar.get('expr0'));
					incount += Integer.valueOf(stcount);

					System.debug('Profile: ' + strname + ' Id: ' + pId);
					System.debug('count: '   + stcount);

					interMap = new Map<String, String>();
					interMap.put('Profile', strname);
					interMap.put('Count', stcount);

					orglist.put(String.valueOf(row), interMap);
					row++;
				}
			}

			orgnumber = String.valueOf(incount);

		//Partner User Distribution
			result = [SELECT count(id), ProfileId
							FROM User
							WHERE Profile.Name LIKE '%SMART Agency%' and IsActive = true
							GROUP BY ProfileId];

			parlist = new Map<String, Map<String, String>>();
			incount = 0;
			for (AggregateResult ar : result)  {
				pId = String.valueOf(ar.get('ProfileId'));
				pro = [SELECT Name FROM Profile WHERE Id =: pId];
				if (pro.size() > 0) {
					strname = pro[0].Name;
					stcount = String.valueOf(ar.get('expr0'));
					incount += Integer.valueOf(stcount);

					System.debug('Profile: ' + strname + ' Id: ' + pId);
					System.debug('count: '   + stcount);

					interMap = new Map<String, String>();
					interMap.put('Profile', strname);
					interMap.put('Count', stcount);

					parlist.put(String.valueOf(row), interMap);
					row++;
				}
			}

			parnumber = String.valueOf(incount);

		//Partner User Login
			loglist = new Map<String, Map<String, String>>();
			row = 0;
			interMap = new Map<String, String>();
			interMap.put('LogStat', 'Logged In');
			incount = Database.countQuery('SELECT count() FROM User WHERE IsActive = true AND Profile.Name LIKE \'%SMART Agency%\' AND LastLoginDate != null');
			interMap.put('Count', String.valueOf(incount));
			loglist.put(String.valueOf(row), interMap);

			row++;
			interMap = new Map<String, String>();
			interMap.put('LogStat', 'Not Logged In');
			incount = Database.countQuery('SELECT count() FROM User WHERE IsActive = true AND Profile.Name LIKE \'%SMART Agency%\' AND LastLoginDate = null');
			interMap.put('Count', String.valueOf(incount));
			loglist.put(String.valueOf(row), interMap);

		//Number of Agencies
			result = [SELECT count(id), BillingCountry
							FROM Account
							GROUP BY BillingCountry];

			agylist = new Map<String, Map<String, String>>();
			row = 0;
			incount = 0;
			for (AggregateResult ar : result)  {
				strname = String.valueOf(ar.get('BillingCountry'));
				stcount = String.valueOf(ar.get('expr0'));
				incount += Integer.valueOf(stcount);

				if (strname == null || strname == '')
					strname = 'Prod';
				else
					strname = 'Test';

				System.debug('Agency Cat: ' + strname);
				System.debug('count: '   + stcount);

				interMap = new Map<String, String>();
				interMap.put('Category', strname);
				interMap.put('Count', stcount);

				agylist.put(String.valueOf(row), interMap);
				row++;
			}

			agynumber = String.valueOf(incount);

		//Partner User Setup Distribution
			tstlist = new Map<String, Map<String, String>>();
			row = 0;
			interMap = new Map<String, String>();
			interMap.put('AUserCat', 'Test');
			incount = Database.countQuery('SELECT count() FROM User WHERE IsActive = true AND Profile.Name LIKE \'%SMART Agency%\' AND Alias LIKE \'%TEST%\'');
			interMap.put('Count', String.valueOf(incount));
			tstlist.put(String.valueOf(row), interMap);

			row++;
			interMap = new Map<String, String>();
			interMap.put('AUserCat', 'Prod');
			incount = Database.countQuery('SELECT count() FROM User WHERE IsActive = true AND Profile.Name LIKE \'%SMART Agency%\' AND (NOT Alias LIKE \'%TEST%\')');
			interMap.put('Count', String.valueOf(incount));
			tstlist.put(String.valueOf(row), interMap);
	}
}