/*************************************************************************************************************
* @name            OTPVerifyAction
* @author          Albert Poon <albert.poon@salesforce.com>
* @created         03 / 06 / 2021
* @description     Provide function to 
* 					- Request for OTP via email or SMS
* 					- Verify the OTP
*
* Changes (version)
* -----------------------------------------------------------------------------------------------------------
*              No.     Date            Author                  Description
*              ----    ------------    --------------------    ----------------------------------------------
* @version     1.0     2021-06-03      Albert Poon         	   initial version
* @version     2.0     2021-08-23      Albert Poon			   implement auto retry
*
**************************************************************************************************************/
global class VerifyOTPAction {
    
    global class VerifyOTPInput {
        @InvocableVariable(label='OTP Setting Profile' description='Relative metadata type profile API Name of Cloudsales_OTP_Setting for this OTP request.' required=true)
        global String otpSettingProfile;
        @InvocableVariable(label='Customer Reference Number' description='Reference Number that belongs to customer who triggered for OTP. Expect to use record ID of SF' required=true)
        global String otpReceiptentReferenceId;
        @InvocableVariable(label='OTP Code' description='The OTP code to verify' required=true)
        global String otpCode;
        @InvocableVariable(label='OTP Receiptent Contact' description='The receipent contact, e.g. eMail or mobile number' required=true)
        global String otpContact;
        @InvocableVariable(label='Dummy Mode' description='Turn on dummy mode for OTP (API call always success)' required=true)
        global boolean dummyMode;
        global VerifyOTPInput() {
        }
    }
    
    global class VerifyOTPOutput {
        @InvocableVariable(label='StatusCode' description='Status Code')
        global String statusCode;
        @InvocableVariable(label='StatusMessage' description='Status Message')
        global String statusMessage;
        global VerifyOTPOutput(String inStatusCode, String inStatusMessage) {
            statusCode=inStatusCode;
            statusMessage=inStatusMessage;
        }
    }
    
    /* Verify OTP entered by the user
     * @param  VerifyOTPInput the OTP setting name in Cloudsales OTP metadata type
     * @return 2 Strings will be returned: 
     * 		   1)Status code
     * 		   2)Status Message
     */
    @InvocableMethod
    public static List<VerifyOTPOutput> verifyOTP(List<VerifyOTPInput> verifyOTPInput) {
        List<VerifyOTPOutput> output =  new List<VerifyOTPOutput>{};
            if(VerifyOTPInput.size()>0){
                
                
                Cloudsales_OTP_Setting__mdt[] OTPSettingMapping = [SELECT OTP_Charset__c, OTP_Digits__c, OTP_Credential_Name__c,
                                                                   Verify_OTP_URL_Path__c, OTP_Type__c, OTP_Validity__c,
                                                                   Request_Type_ID__c, Template_ID__c, URL_Path_Base__c,
                                                                   Connection_Retry__c
                                                                   FROM Cloudsales_OTP_Setting__mdt
                                                                   WHERE QualifiedApiName=:verifyOTPInput[0].otpSettingProfile];
                
                //If OTP Setting metadata type of specific profile is found
                if(OTPSettingMapping!=null && OTPSettingMapping.size()>0){
                    Cloudsales_OTP_Auth_Credential__mdt[] OTPCredentialConfig = [SELECT Client_Id__c, Client_Secret__c,Label, Token_Endpoint__c
                                                                                 FROM Cloudsales_OTP_Auth_Credential__mdt
                                                                                 WHERE Label=:OTPSettingMapping[0].OTP_Credential_Name__c
                                                                                 LIMIT 1];
                    
                    //If OTP Auth Credential metadata type is found
                    if(OTPCredentialConfig!=null && OTPCredentialConfig.size()>0) {
                        Integer retryCount = 1;
                        String token=null;
                        
                        do{
                            //Acquire asccess token
                            token = SmartOTPTokenAcquireUtil.retrieveToken(OTPCredentialConfig[0]);
                            retryCount++;
                        } while((token==null || token.length()<=0) && retryCount<=OTPSettingMapping[0].Connection_Retry__c);
                        
                        if(token!=null && token.length()>0) {
                            Http http = new Http();
                            HttpRequest request = new HttpRequest();
                            
                            request.setEndpoint(OTPSettingMapping[0].URL_Path_Base__c+OTPSettingMapping[0].Verify_OTP_URL_Path__c);
                            
                            //Preparing body
                            Map<String, Object> obj = new Map<String, Object>();
                            obj.put('otpCode', verifyOTPInput[0].otpCode);
                            obj.put('requestTypeId', OTPSettingMapping[0].Request_Type_ID__c);
                            obj.put('requestIdentifier', verifyOTPInput[0].otpReceiptentReferenceId);
                            
                            System.debug('verifyOTP Request : ' + JSON.Serialize(obj));
                            
                            request.setHeader('Content-Length', String.valueOf(JSON.Serialize(obj).length()));
                            request.setHeader('Content-Type', 'application/json');
                            request.setHeader('Connection', 'keep-alive');
                            request.setHeader('Authorization', 'Bearer '+token);
                            request.setTimeout(15000);
                            
                            request.setBody(JSON.Serialize(obj));
                            request.setMethod('POST');
                            
                            String responseCode;
                            HttpResponse response;
                            retryCount=1;
                            
                            try {
                                do {
                                    responseCode='';
                                    
                                    response = http.send(request);
                                    if(response.getStatusCode()==200){
                                        System.debug('VerifyOTP Response RAW : ' + response.getBody());
                                        
                                        //process response
                                        Map<String, Object> responseJson = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                                        
                                        System.debug('VerifyOTP Response : ' + responseJson);
                                        
                                        responseCode=(String)responseJson.get('statusCode');
                                        
                                        if(responseCode=='2000' || responseCode=='2001' || responseCode!='2002' || responseCode!='2003'
                                           || responseCode=='9003' || responseCode=='9004' || responseCode=='9007' || responseCode=='9008') {
                                               output.add(new VerifyOTPOutput((String)responseJson.get('statusCode'),
                                                                              (String)responseJson.get('statusMessage')));
                                           }
                                    }
                                    retryCount++;
                                    
                                } while(response.getStatusCode()!=200 && retryCount<=OTPSettingMapping[0].Connection_Retry__c && 
                                        responseCode!='2000' && responseCode!='2001' && responseCode!='2002' && responseCode!='2003' && 
                                        responseCode!='9003' && responseCode!='9004' && responseCode!='9007' && responseCode!='9008');
                                
                                if((response!=null && response.getStatusCode()!=200) || (responseCode!='2000' && responseCode!='2001' && responseCode!='2002' && responseCode!='2003' && 
                                   responseCode!='9003' && responseCode!='9004' && responseCode!='9007' && responseCode!='9008')) {
                                       System.debug('E17 - Unable to connect to OTP API');
                                       output.add(new VerifyOTPOutput('E17','Unable to connect to OTP API'));
                                   }
                                
                            } catch(System.CalloutException ex){
                                System.debug(ex.getMessage());
                                output.add(new VerifyOTPOutput('E17','Unable to connect to OTP API'));
                            }
                            
                            
                        } else {
                            System.debug('E23 - Unable to acquire token for OTP API');
                            output.add(new VerifyOTPOutput('E23','Configuration profile "' + OTPSettingMapping[0].OTP_Credential_Name__c+'" not found'));
                        }
                    } else {
                        System.debug('E20 - Configuration profile "' + OTPSettingMapping[0].Verify_OTP_URL_Path__c+'" not found');
                        output.add(new VerifyOTPOutput('E20','Configuration profile "' + OTPSettingMapping[0].Verify_OTP_URL_Path__c+'" not found'));
                    }
                } else {
                    System.debug('E03 - Configuration profile "' + verifyOTPInput[0].otpSettingProfile+'" not found');
                    output.add(new VerifyOTPOutput('E03','Configuration profile "' + verifyOTPInput[0].otpSettingProfile+'" not found'));
                }
            } else {
                System.debug('E04 - Empty request is provided');
                output.add(new VerifyOTPOutput('E04','Empty request is provided'));
            }
        return output;
    }
}