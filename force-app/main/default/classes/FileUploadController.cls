/*
 * sfdcmonkey.com 
 * 25/9/2017
 */

public with sharing class FileUploadController {
 
 
    @AuraEnabled
    public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) {
        // check if fileId id ''(Always blank in first chunk), then call the saveTheFile method,
        //  which is save the check data and return the attachemnt Id after insert, 
        //  next time (in else) we are call the appentTOFile() method
        //   for update the attachment with reamins chunks   
        //if (fileId == '') {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        //} else {
        //    appendToFile(fileId, base64Data);
        //}
 
        return Id.valueOf(fileId);
    }
 
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
        System.debug('saveTheFile - 1');
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        System.debug('saveTheFile - 2');
        //Insert ContentVersion
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
        contentVersion.PathOnClient = fileName;//File name with extention
        contentVersion.Origin = 'C';//C-Content Origin. H-Chatter Origin.
        contentVersion.OwnerId = UserInfo.getUserId();//Owner of the file
        contentVersion.Title = fileName;//Name of the file
        contentVersion.VersionData = EncodingUtil.base64Decode(base64Data);//File content
        Insert contentVersion;

        System.debug('saveTheFile - 3 - ' + contentVersion.id); 
        System.debug('saveTheFile - 3 - ' + contentVersion.ContentDocumentId);        
        //After saved the Content Verison, get the ContentDocumentId
        Id contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:contentVersion.Id].ContentDocumentId;
        
        System.debug('saveTheFile - 4');
        //Insert ContentDocumentLink
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.ContentDocumentId = contentDocumentId;//Add ContentDocumentId
        contentDocumentLink.LinkedEntityId = parentId;//Add attachment parentId
        contentDocumentLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        contentDocumentLink.Visibility = 'SharedUsers';//AllUsers, InternalUsers, SharedUsers
        Insert contentDocumentLink;

        System.debug('saveTheFile - 5');
        return contentVersion.Id;
    }
 
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
 
        Attachment a = [
            SELECT Id, Body
            FROM Attachment
            WHERE Id =: fileId
        ];
 
        String existingBody = EncodingUtil.base64Encode(a.Body);
 
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data);
 
        update a;
    }
}