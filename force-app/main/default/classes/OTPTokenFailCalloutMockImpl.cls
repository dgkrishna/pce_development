global class OTPTokenFailCalloutMockImpl implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json; charset=utf-8');
        res.setHeader('X-Content-Type-Options', 'nosniff');
        res.setHeader('X-Frame-Options', 'deny');
        res.setBody('{"access_token":"","expires_in":3600,"token_type":"Bearer","scope":"apigateway openid otp otp_notifications"}');
        res.setStatusCode(400);
        return res;
    }
}