@isTest
public class VerifyOTPActionTest {
	
    //Normal Callout Test
    @isTest static void testNormalCallout(){
        
		Test.setMock(HttpCalloutMock.class, new VerifyOTPAPISuccessCalloutMockImpl());
        
        VerifyOTPAction.VerifyOTPInput voi = new VerifyOTPAction.VerifyOTPInput();
		voi.otpCode='123456';
        voi.otpReceiptentReferenceId='A12345678';
        voi.otpSettingProfile='Smart_eMail';
        voi.otpContact='albert.poon@salesforce.com';
        voi.dummyMode=false;
        
        Test.startTest();
        List<VerifyOTPAction.VerifyOTPOutput> voo = VerifyOTPAction.verifyOTP(new List<VerifyOTPAction.VerifyOTPInput>{voi});
        System.assert(voo!=null);
        System.assert(voo.size()>0);
        System.assertEquals('2000', voo[0].statusCode);
        Test.stopTest();
    }
    
    //Verify Fail Test
    @isTest static void testVerifyFail(){
        
		Test.setMock(HttpCalloutMock.class, new VerifyOTPAPIFailCalloutMockImpl());
        
        VerifyOTPAction.VerifyOTPInput voi = new VerifyOTPAction.VerifyOTPInput();
		voi.otpCode='123456';
        voi.otpReceiptentReferenceId='A12345678';
        voi.otpSettingProfile='Smart_eMail';
        voi.otpContact='albert.poon@salesforce.com';
        voi.dummyMode=false;
        
        Test.startTest();
        List<VerifyOTPAction.VerifyOTPOutput> voo = VerifyOTPAction.verifyOTP(new List<VerifyOTPAction.VerifyOTPInput>{voi});
        System.assert(voo!=null);
        System.assert(voo.size()>0);
        System.assertEquals('9001', voo[0].statusCode);
        Test.stopTest();
    }
}