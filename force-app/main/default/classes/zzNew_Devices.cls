public class zzNew_Devices {
	//GET/SET variables
		public String divHeight {get; set;}

		public String OldDevices {get; set;}
		public String NewDevices {get; set;}

		public Blob csvFileBody	  {get;set;}
		public String csvAsString {get;set;}
		public List<Device_Upload__c> duplist {get;set;}

		public Boolean showSPromptScr {get; set;}
		public Boolean showFUploadScr {get; set;}
		public Boolean showResltScr {get; set;}
		public Boolean showTableScr {get; set;}

		public String  resultMessageE {get; set;}
		public String  resultMessageS {get; set;}

		public String zObjType  = 'Lead';
		public String zPickList = 'Choose_your_Device__c';
		public Boolean recImport;

	public zzNew_Devices() {
		list<User> userlist = [SELECT Id
								FROM User
								WHERE IsActive = true AND Profile.Name = 'SMART Territory Sales Manager User Profile'
								ORDER BY Name ASC];
		//special processing lang to ha! para lang maayos ko yong div wrapper ng table display hehehe
		if (userlist.size() > 40)
			divHeight = '567';		//Prod
		else
			divHeight = '537';		//UAT

		//Get the details of the current picklist -- we need to include/exclude existing values
		SobjectType objType = Schema.getGlobalDescribe().get(zObjType);							//get Object
		Map<String,Schema.SObjectField> objFields = objType.getDescribe().fields.getMap();		//get the Fields
		SObjectField fieldName = objFields.get(zPickList);						//select the picklist
		Schema.DescribeFieldResult fieldResult = fieldName.getDescribe();						//get Describe
		fieldResult = fieldResult.getSObjectField().getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();						//get the values as a list
		//we are getting the labels instead of getValue() because the getValue() gets the API Name.. in the API Name,
		//	we convert all + to _ because the + causes an issue in the update RecordType picklist that fails the update
		for (Schema.PicklistEntry pl : ple) {				//we iterate
			if (pl.isActive()) {							//and only get the active ones
				if (OldDevices != null)
					OldDevices += '~' + pl.getLabel();		//we concatenate all labels delimited by ~
				else
					OldDevices = pl.getLabel();				//ditto
			}
		}

		getNewValues();

		showSPromptScr = true;
	}

	public void getNewValues() {
		NewDevices = null;

		//we read the new device upload custom Object
		List<Device_Upload__c> dup = [SELECT Id, New_Devices__c
					FROM Device_Upload__c
					ORDER BY Id];							//maintain upload order (this will be the order of the new picklist)
		if (dup.size() > 0) {								//iterate through all the records
			for (Device_Upload__c du : dup) {
				if (NewDevices != null)
					NewDevices += '~' + du.New_Devices__c;	//and concatenate all values delimited by ~
				else 										//the values here become the api names if copied to fullName of value
					NewDevices = du.New_Devices__c;
			}

			recImport = false;	//everytime we got something from the New Device upload file we set this to false
								//so that when the back button is clicked.. there will be no unnecessary getting of records again
		}
	}

	public void showUploadCSV() {
		showSPromptScr = false;
		showFUploadScr = true;
	}

	public void gobackToMain() {
		//if our success Import flag is set to true.. we read New Devices object again
		if (recImport == true)
			getNewValues();

		showFUploadScr = false;
		showTableScr   = false;
		showResltScr   = false;
		showSPromptScr = true;
	}

	public void importCSVFile() {
		String[] csvFileLines = new String[] {};
		duplist = New List<Device_Upload__c>();

		csvAsString  = csvFileBody.toString();
		csvFileLines = csvAsString.split('\n');

		String[] csvRecordData = csvFileLines[0].split(',');
		if (csvRecordData[0].trim() == 'New Devices') {
			for (Integer i=1; i<csvFileLines.size(); i++) {
				Device_Upload__c dupObj = new Device_Upload__c() ;
				csvRecordData = csvFileLines[i].split(',');
					dupObj.New_Devices__c = csvRecordData[0].trim();
				//add record to List
				duplist.add(dupObj);
			}

			showResltScr = false;
			showTableScr = true;
		}
		else {
			System.Debug('Maybe wrong file - Error in CSV Header');
			showTableScr = false;
			showResltScr = true;
			resultMessageE = 'You may have uploaded the wrong file. The CSV column header/s is/are not as expected!';
			resultMessageS = '';
		}
	}

	public void createRecords() {
		Delete [SELECT Id FROM Device_Upload__c LIMIT 10000];
		insert duplist;

		showTableScr = false;
		showResltScr = true;
		resultMessageS = 'New Device records were successfully imported from CSV data! Ready to update Device Picklist.. ' +
			'Please click Back button now to update Device Picklist from Main Screen.';
		resultMessageE = '';
		recImport = true;	//everytime we successfully import records from CSV to New Devices object, we set this to true
	}

	public static MdataService2.MetadataPort createService(){
		//instantiate a new MetadataService Port
		MdataService2.MetadataPort service = new MdataService2.MetadataPort();
		service.SessionHeader = new MdataService2.SessionHeader_element();
		service.SessionHeader.sessionId = UserInfo.getSessionId();
		service.timeout_x = 120000;
		return service;
	}

	public void createDevices() {
		System.Debug('we are in createDevices!');
			//System.Debug('oldDevices :>> ' + oldDevices);
			//System.Debug('newDevices :>> ' + newDevices);
		//we convert to array/list our concatenated picklist values (old and new)
			List<String> lstOldDevices = OldDevices.split('~');
			List<String> lstNewDevices = NewDevices.split('~');
			List<String> lstNewOnes = new List<String>();			//this list will contain Devices to be added
			List<String> lstRetains = new List<String>();			//this list will contain Devices to be retained

		//we lookup for values of new devices in the old device array
		for (String newv : lstNewDevices) {
			if (lstOldDevices.indexOf(newv) == -1) {			//if not found -- meaning a NEW DEVICE
				lstNewOnes.add(newv);
				lstRetains.add('N');
			}
			else {
				lstNewOnes.add(newv);							//if found -- meaning retain old device
				lstRetains.add('R');
			}
		}

        MdataService2.MetadataPort service = createService();
		MdataService2.CustomField customField = new MdataService2.CustomField();
		customField.fullName = 'Lead.Choose_your_Device__c';
		customField.label  	 = 'Choose your Device';
		customField.type_x 	 = 'Picklist';

		//Create the valueSet for picklist type
		MdataService2.ValueSet picklistValueSet = new MdataService2.ValueSet();
		//For each ValueSet, we have either ValueSetValuesDefinition or ValueSettings and some other attributes
		MdataService2.ValueSetValuesDefinition valueDefinition = new MdataService2.ValueSetValuesDefinition();
		//Set a new list of values that will contain both NEW and retained devices -- will be the value part of
		//	ValueDefinition which in turn is part of ValueSet
		List<MdataService2.CustomValue> values = new List<MdataService2.CustomValue>();
		//we iterate through our lstNewOnes array (with related lstRetains array -- N=New and R=Retain)
		for (Integer i=0; i<lstNewOnes.size(); i++) {
			MdataService2.CustomValue value = new MdataService2.CustomValue();
			//value.fullName  = lstNewOnes[i].replace('+', '_');	//we need to convert + to _ in the api name
			value.fullName  = lstNewOnes[i];						//not necessay to do convert if recordtype update is
			value.label 	= lstNewOnes[i];						//for label.. the values are as is         not automated
			value.default_x = false ;
			if (lstRetains[i] == 'N')
				value.isActive = true;		//add new picklist values
			else
				value.isActive = null;		//activate old ones that need to be retained
			//add picklist value to our CustomValue set
			values.add(value);
		}

		valueDefinition.value  = values;
		valueDefinition.sorted = false;		//no sorting for picklist values -- as they are created
		//set the valueSetDefinition
		picklistValueSet.valueSetDefinition = valueDefinition;
		//Set the valueSet for picklist type
		customField.valueSet = picklistValueSet;
		//this is the part that actually updates the metadata of the field -- picklist in our case
		if (!Test.isRunningTest()) {
			MdataService2.SaveResult[] results = service.updateMetadata(new List<MdataService2.Metadata> {customField});
		}

		showSPromptScr = false;
		showResltScr = true;
		resultMessageS = 'Device Picklist successfully updated! You need to go to RecordType in Object Manager and select new ' +
			'devices for Smart PFS. Just select all new devices and add to the RecordType picklist.';
		resultMessageE = '';
	}
}