public  class CustomController {
        //Variable Declaration
        public String failingPageResponse { get; set; }

        public PageReference yourPageLevelAction()
       {
          failingPageResponse = Site.getErrorDescription();
          return null;
        }
    }//CustomController