@isTest
public class zTEST_New_Agency_User {

    @isTest static void testMyController() {
		//create TSM User
			Profile prof = [Select Id from Profile where name = 'SMART Territory Sales Manager User Profile'];
			UserRole uRole = [Select Id from UserRole where name = 'SMART PFS TSM 01'];
			User usr = new User(
				ProfileId 		  = prof.Id,
				UserRoleId 		  = uRole.Id,
				Username 		  = System.now().millisecond() + 'xtest2@test.com',
				Alias 			  = 'xbatman',
				Email 			  = 'xxhdkdjhasedfasdfbruce.wayne@wayneenterprises.com',
				EmailEncodingKey  = 'UTF-8',
				Firstname 		  = 'Bruce',
				Lastname 		  = 'Wayne',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey 	  = 'en_US',
				TimeZoneSidKey 	  = 'Asia/Manila'
			);
			insert usr;
			Id withRoleId = usr.Id;

			prof = [Select Id from Profile where name = 'System Administrator'];
			User usr2 = new User();
				usr2.ProfileId 		   = prof.Id;
				usr2.Username  		   = System.now().millisecond() + 'xtest3@test.com';
				usr2.Alias 	  		   = 'thor';
				usr2.Email 			   = 'jfiejdkurldilejhjhfclark.kent@metroglobe.com';
				usr2.EmailEncodingKey  = 'UTF-8';
				usr2.Firstname 		   = 'Bruce';
				usr2.Lastname 		   = 'Wayne';
				usr2.LanguageLocaleKey = 'en_US';
				usr2.LocaleSidKey 	   = 'en_US';
				usr2.TimeZoneSidKey    = 'Asia/Manila';
			insert usr2;


		System.runAs(usr2) {
			//create Account and update it to enable parter
				Account acc = new Account(
					Name 		= 'The New Kid In Town',
					BillingCity = 'Makati',
					Phone 		= '8865-4020',
					OwnerId 	= withRoleId
				);
				insert acc;

				acc.IsPartner = true;
				update acc;

			//create Agency_User_Upload records
				//valid manager
				Agency_User_Upload__c au = new Agency_User_Upload__c(
					Account_Name__c = 'The New Kid In Town',
					First_Name__c 	= 'Bumblebee',
					Last_Name__c	= 'Decepticon',
					Email__c		= 'xxyzzyybumblebee@gmail.com',
					Phone__c		= '9190919191',
					Agent_Code__c	= '632456',
					Role__c 		= 'Agency Manager'
				);
				insert au;
				//valid agent
				au = new Agency_User_Upload__c();
				au.Account_Name__c  = 'The New Kid In Town';
				au.First_Name__c 	= 'Megatron';
				au.Last_Name__c		= 'Autobot';
				au.Email__c 		= 'xxygfehmegatron@gmail.com';
				au.Phone__c 		= '9178634722';
				au.Agent_Code__c 	= '632456';
				au.Role__c 			= 'Agency Agent';
				insert au;
				//valid agent but not included
				au = new Agency_User_Upload__c();
				au.Account_Name__c  = 'The New Kid In Town';
				au.First_Name__c 	= 'Volksmagic';
				au.Last_Name__c		= 'Autobot';
				au.Email__c 		= 'xvolksmagic@gmail.com';
				au.Phone__c 		= '9178634723';
				au.Agent_Code__c 	= '632456';
				au.Role__c 			= 'Agency Agent';
				au.Is_Included__c 	= false;
				insert au;
				//invalid account
				au = new Agency_User_Upload__c();
				au.Account_Name__c  = 'Sa Banda Roon Lang';
				au.First_Name__c 	= 'Falayfay';
				au.Last_Name__c		= 'Fifita';
				au.Email__c 		= 'xsdfgsgstretfalayfay@gmail.com';
				au.Phone__c 		= '9179993344';
				au.Agent_Code__c 	= '632456';
				au.Role__c 			= 'Agency Agent';
				insert au;

			Test.StartTest();
				zzNew_Agency_User controller = new zzNew_Agency_User();

				controller.showUploadCSV();

				controller.gobackToMain();

				Blob bodyBlob = Blob.valueOf('Wrong Header\n');
				controller.csvFileBody = bodyBlob;
				controller.importCSVFile();

				bodyBlob = Blob.valueOf('Email,First Name,Last Name,Phone,Account Name,Role,Agent Code\ntest@test.com,Jose,Rizal,09191919111,Cellphone Repair,Agency Agent,654321');
				controller.csvFileBody = bodyBlob;
				controller.importCSVFile();

				controller.createRecords();

				controller.UserUpInc = 'Yes~Yes~No~Yes';
				controller.createAgencyUsers();
			Test.StopTest();
		}
    }
}