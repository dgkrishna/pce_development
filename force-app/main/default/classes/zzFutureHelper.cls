public class zzFutureHelper {

	@future
	public Static void updateUser(Id usrId, String aName) {
		//this class is called by a method in zzNew_Agency_User.. to update the user record and change role Id to that
		//	of an agency manager -- when created, the default role is that of an agency agent
		System.Debug('We are in FutureHelper-updateUser!');
		System.Debug('usrId :>> ' + usrId);
		System.Debug('aName :>> ' + aName);
		//get the Id of the role (aName has the Name of the Manager role passed by the calling controller)
		String query = 'SELECT Id, Name FROM UserRole WHERE Name LIKE \'%Partner Manager%\' '
			+ ' AND Name LIKE \'%' + aName + '%\' ORDER BY Name ASC';
		UserRole urole = Database.query(query);
		System.Debug('urole.Name :>> ' + urole.Name);
		//then we update the UserRoleId of the user record -- QED!
		User ur = [SELECT Id From User WHERE Id =: usrId];
		ur.UserRoleId = urole.Id;
		Database.update(ur);
	}

	@future
	public Static void deleteContact(Id contId) {
		//this method in the FutureHelper class is called by zzAgent_Inquiry to delete the Contact that is related to the
		//	partner User that was deactivated
		System.Debug('we are in FutureHelper-deleteContact!');
		System.Debug('contId :>> ' + contId);
		//WE WILL DO AN ITERATION HERE USING WHILE BECAUSE SOMETIMES THIS METHOD FIRES BEFORE THE USER RECORD GETS UPDATED!
		Integer num = 1;
		Integer itr = 0;
		List<User> ulist = new List<User>();
		//here is where we do a WHILE -- our iteration limit is
		while (num > 0) {
			System.Debug('itr :>> ' + itr);
			//read any user that has its ContactId equal to the Id passed by the calling controller to this method
			ulist = [SELECT Id, FirstName, LastName, Email FROM User WHERE ContactId =: contId];
			System.Debug('ulist.size() :>> ' + ulist.size());
			num = ulist.size();	//we pass the size of the returned object to num which we test in order to delete the Contact
			if (num == 0)
				delete [SELECT Id FROM Contact WHERE Id =: contId];	//SF doesn't allow deleting a Contact that has a related User
			else {
				//we display via user debug fields of the first record if ever there is or are
				System.Debug('ulist[0].FirstName :>> ' + ulist[0].FirstName);
				System.Debug('ulist[0].LastName :>>  ' + ulist[0].LastName);
				System.Debug('ulist[0].Email :>> 	 ' + ulist[0].Email);
				itr++;			//we increment our loop counter and test it afterwards for the limit
				if (itr == 3)	//we limit our loop to 3x so we won't get scolded by Salesforce hahahahah!
					num = 0;	//we force our loop limiter to 0 regardless of result
			}
		}
	}
}