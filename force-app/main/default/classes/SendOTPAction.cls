/*************************************************************************************************************
* @name            SendOTPAction
* @author          Albert Poon <albert.poon@salesforce.com>
* @created         01 / 06 / 2021
* @description     Provide function to send OTP  
* 	
*
* Changes (version)
* -----------------------------------------------------------------------------------------------------------
*              No.     Date            Author                  Description
*              ----    ------------    --------------------    ----------------------------------------------
* @version     1.0     2021-06-01      Albert Poon			   initial version
* @version     2.0     2021-08-23      Albert Poon			   implement auto retry
*
**************************************************************************************************************/
global class SendOTPAction {
    global class RequestOTPInput {
        @InvocableVariable(label='OTP Setting Profile' description='Relative metadata type profile API Name of Cloudsales_OTP_Setting for this OTP request.' required=true)
        global String otpSettingProfile;
        @InvocableVariable(label='Customer Reference Number' description='Reference Number that belongs to customer who triggered for OTP. Expect to use record ID of SF' required=true)
        global String otpReceiptentReferenceId;
        @InvocableVariable(label='OTP Receiptent Contact' description='The receipent contact, e.g. eMail address or mobile number' required=true)
        global String otpReceiptentContact;
        @InvocableVariable(label='Template Parameters' description='The parameters to populate the template' required=true)
        global List<String> templateParam;
        @InvocableVariable(label='Dummy Mode' description='Turn on dummy mode for OTP (API call always success)' required=true)
        global boolean dummyMode;
        global RequestOTPInput() {
        }
    }
    
    global class RequestOTPOutput {
        @InvocableVariable(label='StatusCode' description='Status Code')
        global String statusCode;
        @InvocableVariable(label='StatusMessage' description='Status Message')
        global String statusMessage;
        global RequestOTPOutput(String inStatusCode, String inStatusMessage) {
            statusCode=inStatusCode;
            statusMessage=inStatusMessage;
        }
    }
    
    /* Send OTP to user to be used by Flow
     * @param  OTPInput the OTP setting name in Cloudsales OTP metadata type
     * @return 2 Strings will be returned: 
     * 		   1)Status code
     * 		   2)Status Message
     */
    @InvocableMethod
    public static List<RequestOTPOutput> sendOTP(List<RequestOTPInput> requestOTPInput) {
        List<RequestOTPOutput> out=new List<RequestOTPOutput>{};
            if(RequestOTPInput.size()>0){
                
                Cloudsales_OTP_Setting__mdt[] OTPSettingMapping = [SELECT OTP_Charset__c, OTP_Digits__c, OTP_Credential_Name__c,
                                                                   Send_OTP_URL_Path__c, OTP_Type__c, OTP_Validity__c,
                                                                   Request_Type_ID__c, Template_ID__c, URL_Path_Base__c,
                                                                   Connection_Retry__c
                                                                   FROM Cloudsales_OTP_Setting__mdt
                                                                   WHERE QualifiedApiName=:requestOTPInput[0].otpSettingProfile];
                
                //If OTP Setting metadata type of specific profile is found
                if(OTPSettingMapping!=null && OTPSettingMapping.size()>0){
                    Cloudsales_OTP_Auth_Credential__mdt[] OTPCredentialConfig = [SELECT Client_Id__c, Client_Secret__c,Label, Token_Endpoint__c
                                                                                 FROM Cloudsales_OTP_Auth_Credential__mdt
                                                                                 WHERE Label=:OTPSettingMapping[0].OTP_Credential_Name__c
                                                                                 LIMIT 1];
                    
                    //If OTP Auth Credential metadata type is found
                    if(OTPCredentialConfig!=null && OTPCredentialConfig.size()>0) {
                        Integer retryCount = 1;
                        String token=null;
                        
                        do {
                        	//Acquire asccess token
                        	token = SmartOTPTokenAcquireUtil.retrieveToken(OTPCredentialConfig[0]);
                            retryCount++;
                        } while((token==null || token.length()<=0)
                               &&(retryCount<=OTPSettingMapping[0].Connection_Retry__c));
                            
                        if(token!=null && token.length()>0) {
                            Http http = new Http();
                            HttpRequest request = new HttpRequest();
                            
                            request.setEndpoint(OTPSettingMapping[0].URL_Path_Base__c+OTPSettingMapping[0].Send_OTP_URL_Path__c);
                            
                            //Preparing body
                            Map<String, Object> body = new Map<String, Object>();
                            body.put('otpDigits', OTPSettingMapping[0].OTP_Digits__c);
                            body.put('otpCharset', OTPSettingMapping[0].OTP_Charset__c);
                            body.put('otpValidity', OTPSettingMapping[0].OTP_Validity__c);
                            body.put('requestTypeId', OTPSettingMapping[0].Request_Type_ID__c);
                            body.put('requestIdentifier', requestOTPInput[0].otpReceiptentReferenceId);
                            
                            List<Map<String, Object>> channels = new List<Map<String, Object>>();
                            Map<String, Object> channel = new Map<String, Object>();
                            channel.put('type', OTPSettingMapping[0].OTP_Type__c);
                            channel.put('targetId', requestOTPInput[0].otpReceiptentContact);
                            channel.put('templateId', OTPSettingMapping[0].Template_ID__c);
                            
                            requestOTPInput[0].templateParam.add(Integer.valueOf(OTPSettingMapping[0].OTP_Validity__c)/60+' minutes');
                            channel.put('templateParams', requestOTPInput[0].templateParam);
                            
                            channels.add(channel);
                            body.put('channels', channels);
                            System.debug('sendOTP Request : ' + JSON.Serialize(body));
                            
                            request.setHeader('Content-Length', String.valueOf(JSON.Serialize(body).length()));
                            request.setHeader('Content-Type', 'application/json');
                            request.setHeader('Connection', 'keep-alive');
                            request.setHeader('Authorization', 'Bearer '+token);
                            request.setTimeout(15000);
                            
                            request.setBody(JSON.Serialize(body));
                            request.setMethod('POST');
                            
                            String responseCode;
                            HttpResponse response;
                            retryCount=1;
                            
                            try {
                                do {
                                    responseCode='';
                                    
                                    response = http.send(request);
                                    if(response.getStatusCode()==200){
                                        System.debug('sendOTP Response RAW : ' + response.getBody());
                                        
                                        //process response
                                        List<Object> responseJson = (List<Object>)JSON.deserializeUntyped(response.getBody());
                                        
                                        System.debug('sendOTP Response : ' + responseJson[0]);
                                        
                                        responseCode=(String)((Map<String, Object>)responseJson[0]).get('statusCode');
                                        
                                        if(responseCode=='1000' || responseCode=='9003' || responseCode=='9004' ||
                                           responseCode=='9007' || responseCode=='9008') {
                                               out.add(new RequestOTPOutput((String)((Map<String, Object>)responseJson[0]).get('statusCode'),
                                                                            (String)((Map<String, Object>)responseJson[0]).get('statusMessage')));
                                           }
                                    }
                                    retryCount++;
                                } while(response.getStatusCode()!=200 && retryCount<=OTPSettingMapping[0].Connection_Retry__c && 
                                        responseCode!='1000' && responseCode!='9003' && responseCode!='9004' && responseCode!='9007' &&
                                        responseCode!='9008');
                                
                                if((response!=null && response.getStatusCode()!=200) || (responseCode!='1000' && 
                                   responseCode!='9003' && responseCode!='9004' && responseCode!='9007' && responseCode!='9008')) {
                                       System.debug('E16 - Unable to connect to OTP API');
                                       out.add(new RequestOTPOutput('E16','Unable to connect to OTP API'));
                                   }
                                
                            } catch(System.CalloutException ex){
                                System.debug(ex.getMessage());
                                out.add(new RequestOTPOutput('E16','Unable to connect to OTP API'));
                            }
                            
                            
                        } else {
                            System.debug('E22 - Unable to acquire token for OTP API');
                            out.add(new RequestOTPOutput('E22','Configuration profile "' + OTPSettingMapping[0].OTP_Credential_Name__c+'" not found'));
                        }
                    } else {
                        System.debug('E19 - Configuration profile "' + OTPSettingMapping[0].Send_OTP_URL_Path__c+'" not found');
                        out.add(new RequestOTPOutput('E19','Configuration profile "' + OTPSettingMapping[0].Send_OTP_URL_Path__c+'" not found'));
                    }
                } else {
                    System.debug('E01 - Configuration profile "' + requestOTPInput[0].otpSettingProfile+'" not found');
                    out.add(new RequestOTPOutput('E01','Configuration profile "' + requestOTPInput[0].otpSettingProfile+'" not found'));
                }
            } else {
                System.debug('E02 - Empty request is provided');
                out.add(new RequestOTPOutput('E02','Empty request is provided'));
            }
        return out;
    }
}