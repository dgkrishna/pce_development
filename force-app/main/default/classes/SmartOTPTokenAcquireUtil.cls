/*************************************************************************************************************
* @name            Smart OTP Client Credentials Authenticator
* @author          Albert Poon <albert.poon@salesforce.com>
* @created         02 / 06 / 2021
* @description     Provide function to retrieve OAuth2 access token from Smart OTP service via Client Credential Grant
*
* Changes (version)
* -----------------------------------------------------------------------------------------------------------
*              No.     Date            Author                  Description
*              ----    ------------    --------------------    ----------------------------------------------
* @version     1.0     2021-06-01      Albert Poon         	initial version
*
**************************************************************************************************************/
public class SmartOTPTokenAcquireUtil {
    public static String retrieveToken(Cloudsales_OTP_Auth_Credential__mdt config) {
        
        final String body ='client_id=' + EncodingUtil.urlEncode(config.Client_Id__c, 'UTF-8') + 
            '&client_secret=' + EncodingUtil.urlEncode(config.Client_Secret__c, 'UTF-8') +
            '&grant_type=client_credentials';
        System.debug('Token endpoint <' + config.Token_Endpoint__c + '>');
        System.debug('Token Body <' + body + '>');
        
        // setup request
        HttpRequest req = new HttpRequest();
        req.setEndpoint(config.Token_Endpoint__c);
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');        
        //req.setHeader('Authorization', 'Basic '+EncodingUtil.base64Encode(Blob.valueOf(config.get('Client_Id__c')+':'+config.get('Client_Secret__c'))));
        req.setHeader('Content-Length',String.valueOf(body.length()));
        req.setHeader('Connection','keep-alive');
        req.setBody(body);
        req.setMethod('POST');
        
        System.debug('OAuth Request - '+req);
        
        // execute requets
        Http h = new Http();
        HttpResponse res = h.send(req);
        
        System.debug('OAuth Response - '+res);
        
        // get token from response
        final Map<String, Object> data = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        final String errorCode = (String) data.get('error');
        if (String.isNotEmpty(errorCode) || res.getStatusCode()!=200) {
            return null;
        } else {
            return (String) data.get('access_token');
        }
    }
}