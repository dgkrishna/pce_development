@isTest
public class zTEST_New_Devices {

	@TestSetup
	static void makeData(){
		//create New Device records
		Device_Upload__c du = new Device_Upload__c(
			New_Devices__c = 'ZZBlueberry New Generation'
		);
		insert du;
		du = new Device_Upload__c(
			New_Devices__c = 'ZZSamson New Generation'
		);
		insert du;
	}

    // private class RestMock implements HttpCalloutMock {
		//     public HTTPResponse respond(HTTPRequest req) {
		//         String fullJson = 'your Json Response';

		//         HTTPResponse res = new HTTPResponse();
		//         res.setHeader('Content-Type', 'text/json');
		//         res.setBody(fullJson);
		//         res.setStatusCode(200);
		//         return res;
		//     }
		// }

    @isTest static void testMyController() {

		//Test.setMock(HttpCalloutMock.class, new RestMock());
		Test.StartTest();
			zzNew_Devices controller = new zzNew_Devices();

			controller.showUploadCSV();

			controller.gobackToMain();

			Blob bodyBlob = Blob.valueOf('Wrong Header\n');
			controller.csvFileBody = bodyBlob;
			controller.importCSVFile();

			bodyBlob = Blob.valueOf('New Devices\nNew Device A\nNew Device B');
			controller.csvFileBody = bodyBlob;
			controller.importCSVFile();

			controller.createRecords();

			controller.createDevices();
		Test.StopTest();
    }
}