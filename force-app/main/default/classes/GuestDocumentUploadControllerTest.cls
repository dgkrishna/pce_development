@isTest
public class GuestDocumentUploadControllerTest {
  
  //getAllDocuments positive test
    @isTest static void testGetAllDocuments(){
        Test.startTest();
        Lead lead = new Lead();
        lead.FirstName = 'Test First Name';
        lead.LastName = 'Test Last Name';
        lead.Company = 'Test Company';
        lead.MobilePhone='0912341234';
        insert lead;
        
        Blob bodyBlob=Blob.valueOf('Unit Test File Body');
        
        ContentVersion contentVersion_1 = new ContentVersion(
            Title='SampleTitle', 
            PathOnClient ='SampleTitle.jpg',
            VersionData = bodyBlob, 
            origin = 'H'
        );
        insert contentVersion_1;
        
        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId 
                                           FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = lead.id;
        contentlink.contentdocumentid = contentVersion_2.contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        
        Guest_Flow_Document__mdt docuType = [SELECT ID 
                                             FROM Guest_Flow_Document__mdt 
                                             WHERE Document_Name__c='DEPED Employee ID'	
                                             LIMIT 1];
        
        list<String> conVerIds = new List<String>();
        conVerIds.add(contentVersion_2.contentdocumentid);
        
        GuestDocumentUploadController.updateDocuments(docuType.Id, conVerIds);
        
        List<GuestDocumentUploadController.DocumentWrapper> dw = GuestDocumentUploadController.getAllDocuments(lead.id);
        System.assert(dw.size()>0);
        
        List<GuestDocumentUploadController.DocumentWrapper> dw2 = GuestDocumentUploadController.getDocuments(lead.id,'Identification Document');
        System.assert(dw2.size()>0);
        
        Test.stopTest();
    }

}